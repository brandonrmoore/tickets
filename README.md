```
___________.__        __           __          
\__    ___/|__| ____ |  | __ _____/  |_  ______
  |    |   |  |/ ___\|  |/ // __ \   __\/  ___/
  |    |   |  \  \___|    <\  ___/|  |  \___ \
  |____|   |__|\___  >__|_ \\___  >__| /____  >
                   \/     \/    \/          \/
```
# Tickets: A Kanban Board for Businesses #

## Symfony (Back-End) Docs ##

### Developers: Getting Started ###
1. Install Composer. For more information [click here](https://getcomposer.org/doc/00-intro.md)

2. Install the project's (local) dependencies using Composer:
```
composer install
```
3. Start your MariaDB SQL service(s):
```
// For example, for Mac/OSX:
mysql.server start
// To auto-start MariaDB Server, use Homebrew's services functionality:
brew services start mariadb
```

4. Start PHP local development server:
```
php bin/console server:start --force
```
5. Code! Use the files inside the `./src` folder.
Live reload on: `localhost:8000` by default.

## Angular 4 (Front-End) Docs ##

### Developers: Getting Started ###
1. Install NodeJs and NPM. For more information [click here](https://nodejs.org/en/download/package-manager/)
2. Install global NPM dependencies:
```
sudo npm install -g @angular/cli
```
*Note: Make sure that you are using the latest @angular/cli, Angular 4 compatible.*

3. Install the project's (local) NPM dependencies:
```
cd frontend/
npm install
```
4. Start development server:
```
ng serve
```
5. Code! Use the files inside the `./frontend` folder.
Live reload on: `localhost:4200` by default.

## API Documentation
Uses Nelmio\ApiDocBundle\NelmioApiDocBundle installed via composer

HTML Routes are not used except for in dev environments for security reasons.
See [dev routing file](app/config/routing_dev.yml)

For end users the documentation is exposed via a securely managed route
and in JSON format for Angular to style.
It should live in the **api_docs** folder and generated as dictated below:
> php bin/console api:doc:dump --format=json > api_docs/v1.json

## PHP Docs
The binary for PHPDoc is committed with the repo. It can be ran as dictated below:
> ./phpdoc -c phpdoc.tpl.xml

## PHP Unit Testing
The executable for this is installed via composer and a symlink is committed in git to point to **vendor/phpunit/phpunit/phpunit**
It can be ran via:
> ./phpunit -c phpunit.xml.dist
