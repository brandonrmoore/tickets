<?php
$container->setParameter('debug_toolbar', true);
$container->setParameter('debug_redirects', true);
if(!isset($_SERVER['HTTP_HOST']) ) {
    $arr = explode('/',(isset($_SERVER['PWD']) ? $_SERVER['PWD'] : '/'));
    switch($arr[2]){
        case "jorge-tickets":
            $_SERVER['HTTP_HOST'] = 'jorge-tickets.com';
            break;
        case "jorgelison":
        case "yoryo":
        case "brandonmoore":
            $_SERVER['HTTP_HOST'] = '127.0.0.1:8000';
            break;
        case "brandon-tickets":
            $_SERVER['HTTP_HOST'] = 'brandon-tickets.com';
            break;
    }
}
switch($_SERVER['HTTP_HOST']){
    case "www.jorge-tickets.com":
    case "jorge-tickets.com":
        $container->setParameter('database_host', '127.0.0.1');
        $container->setParameter('database_name', 'jorge_tickets');
        $container->setParameter('database_user', 'jorge');
        $container->setParameter('database_password', 'jorge');
        $container->setParameter('database_port', '3306');
        break;
    case "brandon-tickets.com":
    case "www.brandon-tickets.com":
        $container->setParameter('database_host', '127.0.0.1');
        $container->setParameter('database_name', 'brandon_tickets');
        $container->setParameter('database_user', 'brandon');
        $container->setParameter('database_password', 'brandon');
        $container->setParameter('database_port', '3306');
        break;
    case '127.0.0.1:8000':
        $container->setParameter('database_host', '127.0.0.1');
        $container->setParameter('database_name', 'tickets');
        $container->setParameter('database_user', 'admin');
        $container->setParameter('database_password', 'admin');
        $container->setParameter('database_port', '3306');
        break;
}
$container->setParameter('mailer_transport', 'smtp');
$container->setParameter('mailer_host', null);
$container->setParameter('mailer_user', null);
$container->setParameter('mailer_password', null);
$container->setParameter('secret', '29393240e140c919ae325f97d73cca5bca765738');
