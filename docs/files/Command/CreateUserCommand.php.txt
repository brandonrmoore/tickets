<?php
/**
 * Class CreateUserCommand
 *
 * @package     CoreBundle
 * @subpackage  Command
 */
namespace CoreBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
/**
 * Class CreateUserCommand - Command for CLI to add a new user
 *
 * Creates a new Users entity
 */
class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * Set up command
     *
     * @category Commands
     * @return null
     */
    protected function configure()
    {
        $this->setName('app:create-user')
            ->setDescription('Creates a user')
            ->setHelp('No help needed.')
            ->addArgument('username', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
            ->addArgument('isAdmin', InputArgument::REQUIRED, '1 or 0 for isAdmin');
    }

    /**
     * Execute command with data input on CLI
     *
     * @category Commands
     * @param \Symfony\Component\Console\Input\InputInterface $input CLI input interface
     * @param \Symfony\Component\Console\Output\OutputInterface $output CLI output interface
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);
        // retrieve the argument value using getArgument()
        $em = $this->getContainer()->get('doctrine')->getManager();
        $user = new \CoreBundle\Entity\Users;
        $user->setUsername($input->getArgument('username'));
        $encoder = $this->getContainer()->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $input->getArgument('password'));
        $user->setPassword($password);
        $user->setEmail($input->getArgument('username'));
        $user->setIsActive(true);
        $user->setIsAdmin($input->getArgument('isAdmin') ? true : false);
        $em->persist($user);
        $em->flush();
    }
}
