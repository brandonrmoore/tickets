<?php
/**
 * Class Groups
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * Groups
 */
class Groups
{
    /**
     * @var string group name
     * @ApiMeta(description="The name of the Group")
     */
    private $name;

    /**
     * @var integer unique ID
     * @ApiMeta(description="Primary key")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection users assocated with this group
     * @ApiMeta(description="Users for this Group")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $groupUsers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name set the group name
     *
     * @return Groups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add groupUser
     *
     * @param \CoreBundle\Entity\Users $groupUser associated user to add
     *
     * @return Groups
     */
    public function addGroupUser(\CoreBundle\Entity\Users $groupUser)
    {
        $this->groupUsers[] = $groupUser;

        return $this;
    }

    /**
     * Remove groupUser
     *
     * @param \CoreBundle\Entity\Users $groupUser user to remove
     */
    public function removeGroupUser(\CoreBundle\Entity\Users $groupUser)
    {
        $this->groupUsers->removeElement($groupUser);
    }

    /**
     * Get groupUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupUsers()
    {
        return $this->groupUsers;
    }

    /**
     * Get groupUsers as array
     *
     * @return array
     */
    public function getGroupUsersArray()
    {
        $ids = [];
        foreach($this->groupUsers AS $user){
            $ids[] = $user->getId();
        }
        return $ids;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection tickets assigned to this group
     * @ApiMeta(description="Tickets associated with this Group")
     * @JMSAnnotation\MaxDepth(1)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $groupTickets;


    /**
     * Add groupTicket
     *
     * @param \CoreBundle\Entity\Tickets $groupTicket
     *
     * @return Groups
     */
    public function addGroupTicket(\CoreBundle\Entity\Tickets $groupTicket)
    {
        $this->groupTickets[] = $groupTicket;

        return $this;
    }

    /**
     * Remove groupTicket
     *
     * @param \CoreBundle\Entity\Tickets $groupTicket
     */
    public function removeGroupTicket(\CoreBundle\Entity\Tickets $groupTicket)
    {
        $this->groupTickets->removeElement($groupTicket);
    }

    /**
     * Get groupTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupTickets()
    {
        return $this->groupTickets;
    }
}

