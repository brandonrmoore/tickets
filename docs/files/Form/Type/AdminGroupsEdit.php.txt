<?php
/**
 * Class AdminGroupsEdit
 *
 * @package     CoreBundle
 * @subpackage  Form\Type
 */
namespace CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AdminGroupsEdit
 *
 * Generates a form for the admin roles
 */
class AdminGroupsEdit extends AbstractType
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Controller\Controller $container The symfony container object
     */
    private $container;
    /**
     * @var \Doctrine\ORM\EntityManager $em The Doctrine entity manager object
     */
    private $em;
    /**
     * @var \CoreBundle\Entity\Users $user The user entity for this user using the form
     */
    private $user;

    /**
     * Build the form
     *
     * @category Form Builders
     * @param \Symfony\Component\Form\FormBuilderInterface $builder Form Builder interface for SYmfony
     * @param array $options Options to be passed into the form builder
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $this->container = $options['container'];
        $this->em = $options['entity_manager'];
        $entity = $builder->getData();
        $builder
            ->add('name', TextType::class, array('label' => 'Name'))
            ->add('groupUsers', EntityType::class,
                array(
                    'class' => 'CoreBundle:Users',
                    'choice_label' => 'username',
                    'choice_value' => 'id',
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false,
                    'data' => $entity ? $this->getCurrentGroupUsers($entity->getId()) : null
                )
            )
        ;
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            if(method_exists($this, 'preSubmit')){
                $this->preSubmit($event);
            }
        });
    }

    /**
     * Get users for supplied scrum entity
     *
     * @param \CoreBundle\Entity\Groups $group The group entity to get users for
     * @return array
     */
    private function getCurrentGroupUsers($group)
    {
        return $this->em->getRepository('CoreBundle:Groups')->findGroupUsers($group);
    }

    /**
     * Prepend form elements with this string
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver default options resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'buttons'        => array(),
            'options'        => array(),
            'mapped'         => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'form_actions';
    }

    /**
     * Handle options and set required ones
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     * @return null
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Groups'
        ));
        $resolver->setRequired('container');
        $resolver->setRequired('entity_manager');
        $resolver->setRequired('user');
    }
}
