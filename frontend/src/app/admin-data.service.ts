import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class AdminDataService {

  private host: string;
  private api: any;
  private develop: any;

  constructor(private http: Http) {
    this.host = 'http://127.0.0.1:4200';
    this.api = {
      admin : {
        form: {
          users: {
            get : '/v1/admin/get/form/users/edit',
            put: '/v1/admin/put/form/users/edit'
          }
        },
        data: {
          users: '/v1/admin/get/data/users',
          groups: '/v1/admin/get/data/groups',
          scrums: '/v1/admin/get/data/scrums'
        }
      }
    };
    this.develop = {
      admin : {
        form: {
          users: {
            get : '/assets/users-edit.json',
            put: '/v1/admin/put/form/users/edit'
          }
        },
        data: {
          users: '/assets/users-list.json',
          groups: '/assets/groups-list.json',
          scrums: '/assets/scrums-list.json'
        }
      }
    };
  }

  getFields() {
    console.log('Getting form fields...');
    return this.http.get(this.host + this.develop.admin.form.users.get)
               .map(res => res.json());
              //  .subscribe((value) => {
              //    console.log('Observable value is: %j', value);
              //  });
              // .map(data => data.items);
  }

  getUsers() {
    console.log('Getting users...');
    return this.http.get(this.host + this.develop.admin.data.users)
               .map(res => res.json());
  }

  getGroups() {
    console.log('Getting groups...');
    return this.http.get(this.host + this.develop.admin.data.groups)
          .map(res => res.json());
  }

  getScrums() {
    console.log('Getting scrums...');
    return this.http.get(this.host + this.develop.admin.data.scrums)
               .map(res => res.json());
  }

  saveUserEdit(user) {
    return this.http.post(
      this.host + this.api.admin.form.users.put,
      JSON.stringify(user)
    ).map(res => res.json());
  }

  saveUserNew(user) {
    return this.http.post(
      this.host + this.api.admin.form.users.put,
      JSON.stringify(user)
    ).map(res => res.json());
  }

}
