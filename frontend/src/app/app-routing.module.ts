import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserAddComponent } from './users/add/add.component';
import { GroupsComponent } from './groups/groups.component';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'users', component: UsersComponent,
        children : [
          { path: '', redirectTo: 'users', pathMatch: 'full' },
          { path: 'add', component: UserAddComponent }
        ]
      },
      { path: 'groups', component: GroupsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
