import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { UserAddComponent } from './users/add/add.component';
import { BoolIconPipe } from './bool-icon.pipe';
import { GroupsComponent } from './groups/groups.component';
import { ScrumsComponent } from './scrums/scrums.component';
import { GroupingPipe } from './grouping.pipe';
import { AvatarPipe } from './avatar.pipe';
import { ListPipe } from './list.pipe';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    LoginComponent,
    SettingsComponent,
    UserAddComponent,
    BoolIconPipe,
    GroupsComponent,
    ScrumsComponent,
    GroupingPipe,
    AvatarPipe,
    ListPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
