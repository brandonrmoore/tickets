import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'avatar'
})
export class AvatarPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value){
      return value;
    }

    return 'assets/common/imgs/default-avatar.svg';
  }

}
