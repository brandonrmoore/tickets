import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'boolIcon'
})
export class BoolIconPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      return 'fa fa-check fa-2x true';
    } else {
      return 'fa fa-times fa-2x false';
    }
  }

}
