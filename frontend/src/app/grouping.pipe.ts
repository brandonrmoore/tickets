import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'grouping'
})
export class GroupingPipe implements PipeTransform {

  transform(value: Array<Object>, groups: any): any {

    const matches = [];

    for (let i = 0; i < value.length; i++) {
      groups.forEach((item) => {
        if (Number(item.id) === Number(value[i])) {
          matches.push(item.name);
        }
      });
    }

    if (matches.length > 0) {
      return matches;
    } else {
      return value;
    }

  }

}
