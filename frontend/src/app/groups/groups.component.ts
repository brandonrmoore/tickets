import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { Observable } from 'rxjs/Observable';
import * as $ from 'jquery';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
  providers: [AdminDataService]
})
export class GroupsComponent implements OnInit, OnDestroy {

  public users: Observable<Array<any>>;
  public groups: any;
  public selectedGroup: any;
  public selectedUsers: any;
  public addUserBtnText: string;
  public addon: any;
  public trash: any;
  public confirmDel: boolean;
  public confirmAdd: boolean;

  constructor(public data: AdminDataService) {
    this.data = data;
    this.trash = false;
    this.confirmAdd = false;
    this.confirmDel = false;
    this.addUserBtnText = 'user';
  }

  ngOnInit() {
    // Get users.
    this.data.getUsers().subscribe(
      // On success
      user => this.users = user,
      // On error
      error => console.log("Error: ", error),
      // On complete
      () => {
        setTimeout(() =>{
          //(<any>jQuery(".chosen-select")).chosen();
          const lists = document.querySelectorAll('.group-card .add-user');
          for (let i = 0; i < lists.length; i++) {
            lists[i].className = 'add-user form-group hide';
          }
        }, 50);
      }
    );

    // Get user groups information.
    this.groups = this.data.getGroups();
  }

  ngOnDestroy() { }

  /**
   * Function to `trackBy` user list in ngFor
   * @private
   * @param  {number} index - Index number.
   * @param  {any} obj - User list.
   * @return {any} - Index.
   */
  trackByIndex(index: number, obj: any): any {
    return index;
  }

  showUsers(id: number){
    console.log('Adding to group: %j', id);
    // Select current group card.
    const group = document.querySelector(`#group-card-${id}`);

    const display = group.getElementsByClassName('display-data');
    const list = group.getElementsByClassName('add-user');

    for (let i = 0; i < list.length; i++) {
      list[i].className = 'add-user form-group';
    }
  }

  add(name: any) {
    console.log('Adding group: %j', name);
    console.log('Groups: %j', typeof this.groups);

    /**
     * @todo
     * 1. Check if group name is already in use.
     * 2. Call API
     * 3. Refresh with values from the API
     */
    //(<any>$(".chosen-select")).chosen();
  }

  /**
   * Open add user modal for a particular group.
   * @param  {Object} group - Group object to add the user to.
   */
  addUserModal(group: any) {
    const self = this;

    // Reset vars.
    this.addUserBtnText = 'user';
    this.selectedUsers = [];
    this.selectedGroup = group;
    this.confirmAdd = false;

    /**
     * @todo
     * Add a proper callback after ngfor has finished.
     */
    setTimeout(() =>{
      (<any>jQuery('#selectUsers')).chosen()
      .val('').trigger("chosen:updated").change(function(){
        self.selectUsers(jQuery(this).val());
      });
    }, 200);
  }

  delete(group: any) {
    console.log('Deleting group: %j', group);
    // Reset previous confirmations
    this.confirmDel = false;
    // Asign group to delete
    this.trash = group;
  }

  /**
   * Enable group name edition for a particular group.
   * @param  {number} id - Group's ID.
   */
  edit(id: number){
    console.log("Id is: %j", id);
    // Select current group card.
    const group = document.querySelector(`#group-card-${id}`);

    // Select all form input-data in every group card.
    const allCards = document.querySelectorAll('.group-card');
    const allDisplays = document.querySelectorAll('.group-card .display-data');
    const allInputs =  document.querySelectorAll('.group-card .input-data');

    // Select current group inputs
    const display = group.getElementsByClassName('display-data');
    const inputs = group.getElementsByClassName('input-data');

    // Reset to display all the data in all the cards.
    for (let i = 0; i < allDisplays.length; i++) {
      allDisplays[i].className = 'display-data';
    }

    // Reset to hide all the edit input fields in all the cards.
    for (let i = 0; i < allInputs.length; i++) {
      allInputs[i].className = 'input-data hide';
    }

    // Display edit input field in the current group card only.
    // Hide actual name display.
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].className = 'input-data';
      display[i].className = 'display-data hide';
    }
  }

  confirmDeletion(event: any) {
    const input = String(event.target.value.toLowerCase());
    const name = String(this.trash.name.toLowerCase());

     if (input === name) {
       this.confirmDel = true;
       return true;
     } else {
       this.confirmDel = false;
       return false;
     }
  }

  selectUsers(users: any) {
    // Update model manually
    this.selectedUsers = users;

    // Show user or users in add user button text
    this.addUserBtnText = (Array.isArray(this.selectedUsers) &&
    this.selectedUsers.length > 1) ? 'users' : 'user';

    // Enable or disable add user button
    this.confirmAdd = (Array.isArray(this.selectedUsers) &&
    this.selectedUsers.length > 0) ? true : false;

  }

  processGroupDel(group: any) {
    if (this.confirmDel === true) {
      console.log('HTTP POST to API in order to delete group...');
    } else {
      console.error('Deletion has not been confirmed by typing ' +
      'the group name in deletion modal');
    }
  }

  processUserAdd(users: any) {
    if (this.confirmAdd === true) {
      console.log('HTTP POST to API in order to add users...');
      console.log('Adding: %j', users);

      /**
       * @todo
       * On success callback from the API:
       * 1) Update group model
       * 2) Reset chose list
       */
       // (<any>jQuery('#selectUsers')).val('').trigger("chosen:updated");
    } else {
      console.error('User(s) addon has not been confirmed in the DOM');
    }
  }


}
