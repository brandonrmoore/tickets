import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'list'
})
export class ListPipe implements PipeTransform {

  transform(value: Array<string>, args?: any): any {
    let list = '<ul class="value-list">';

    if (value.constructor === Array) {
      value.forEach((item) => {
        list += `<li>${item}</li>`;
      });
    }

    list += '</ul>';

    return list;
  }

}
