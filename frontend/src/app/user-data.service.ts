import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UserDataService {

  private api: any;

  constructor(private http: Http) {
    this.api = {
      data: {
        avatar: '/v1/avatar/'
      }
    }
  }

  /**
   * Post an avatar image to the API
   * @param  {Object} formData - Image data appended to the FormData interface.
   * @return {Object} - JSON object with success or error answer from the API.
   */
  postAvatar(formData){
    // Generate Request Headers
    let headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });

    // HTTP Post Request
    return this.http.post(`${this.api.data.avatar}`, formData, options)
               .map(res => res.json());
  }

}
