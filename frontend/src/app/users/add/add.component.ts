import { Component, OnInit, OnDestroy, HostListener, AfterViewChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AdminDataService } from '../../admin-data.service';
import { UserDataService } from '../../user-data.service';
import { UsersComponent } from '../users.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [AdminDataService]
})
export class UserAddComponent extends UsersComponent implements OnInit, OnDestroy, AfterViewChecked {

  public fields: Observable<Array<any>>;

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Pop event happened!');
     this.displayAddBtn();
   }

  constructor(
    public sanitizer: DomSanitizer,
    public data: AdminDataService,
    public userData: UserDataService,
    public route: ActivatedRoute,
    public router: Router) {
      super(sanitizer, data, userData, route, router);
  }

  ngOnInit() {
    this.fields = this.data.getFields()
        .map(fields => {
          for (const key in fields) {
            if (fields.hasOwnProperty(key)) {
              if (fields[key].name === 'password') {
                const insertion = Number(key) + 1;
                fields.splice(insertion, 0, {
                  'name': 'passwordRetype',
                  'type': 'password',
                  'value': null,
                  'required': false,
                  'disabled': false,
                  'attributes': {
                    'required': false,
                    'label': 'Repeat password'
                  }
                });
              }
            }
          }
          return fields;
        });

  }

  /**
   * Chosen jQuery library needs to be initialized after Angular checks the
   * component's views and child views. More information about Angular lifecycle
   * hooks on: https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html
   */
  ngAfterViewChecked() {
    // Use '<any>' to avoid syntax errors on compilation.
    // https://stackoverflow.com/questions/24984014/
   (<any>$(".chosen-select")).chosen();
  }

  add(user: any): any {
    return this.data.saveUserNew(user);
  }

}
