import { Component, OnInit, OnDestroy, HostListener,
  AfterViewChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { AdminDataService } from '../admin-data.service';
import { UserDataService } from '../user-data.service';
import { Router } from '@angular/router';
import * as validator from 'validator';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [AdminDataService, UserDataService]
})
export class UsersComponent implements OnInit, OnDestroy, AfterViewChecked {

  public params: any;
  public sub: any;
  public users: Observable<Array<any>>;
  public groups: any;
  public avatars: any;
  public scrums: Observable<Array<any>>;
  public show: any;
  public trash: any;
  public delEmail: string;
  public defaultAvatar: string;

  constructor(
    public sanitizer: DomSanitizer,
    public data: AdminDataService,
    public userData: UserDataService,
    public route: ActivatedRoute,
    public router: Router) {
      this.data = data;
      this.userData = userData;
      this.show = {
        addbtn: true,
        delbtn: false
      };
      this.trash = false;
      this.delEmail = '';
      this.defaultAvatar = '/assets/common/imgs/default-avatar.svg';
  }

  /**
   * Angular lifecycle-hook to initialize the directive/component after
   * Angular first displays the data-bound properties and sets the
   * directive/component's input properties.
   * @private
   * @return {Null}
   */
  ngOnInit() {
    // Get users.
    this.users = this.data.getUsers();
    // Get user groups information.
    this.data.getGroups().subscribe(group => this.groups = group);
    // Get user scrums information.
    this.data.getScrums().subscribe(scrum => this.scrums = scrum);
    this.getRouteParams();
    this.displayAddBtn();
  }

  /**
   * Angular lifecycle-hook to respond after Angular checks the component's
   * views and child views.
   * @private
   * @see Chosen jQuery library needs to be initialized after Angular checks the
   * component's views and child views. More information about Angular lifecycle
   * hooks on: https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html
   * @return {Null}
   */
  ngAfterViewChecked() {
    // Use '<any>' to avoid syntax errors on compilation.
    // https://stackoverflow.com/questions/24984014/

    // To avoid `chosen is not a function within webpack` bug use `jQuery`
    // instead of `$` to ensure that you call jQuery from the global scope.

    //(<any>jQuery(".chosen-select")).chosen();
  }

  /**
   * Angular lifecycle-hook to cleanup just before Angular destroys the
   * directive/component. Unsubscribe Observables and detach event handlers to
   * avoid memory leaks.
   * @private
   * @return {Null}
   */
  ngOnDestroy() {

    /**
     * @todo unsubscribe from observables
     */
    // this.sub.unsubscribe();
    // this.users.unsubscribe();
  }

  /**
   * Function to `trackBy` user list in ngFor
   * @private
   * @param  {number} index - Index number.
   * @param  {any} obj - User list.
   * @return {any} - Index.
   */
  trackByIndex(index: number, obj: any): any {
    return index;
  }

  /**
   * Front end user edit input validation
   * @param  {Object} user - Object containing user edit input information.
   * @return {Object} - Object containing success/error information.
   */
  validation(user) {

    // Validate email
    if (!validator.isEmail(user.email)) {
      return {
        ok: false,
        msg: 'Email is not valid.'
      };
    }

    return {
      ok: true,
      msg: 'Validation was successful.'
    };

  }

  /**
   * Get route url params
   * @return {Object} - URL/route params
   */
  getRouteParams() {
    this.sub = this.route.params.subscribe(params => {
      this.params = params;
    });
  }

  /**
   * Display add user button in the UI when appropriate.
   * @return {Null}
   */
  displayAddBtn() {
    const subpath = (this.route.snapshot.firstChild) ?
      this.route.snapshot.firstChild.url[0].path : false;

    this.show.addbtn = (subpath === 'add') ? false : true;
    console.log('this.show.addbtn is: %j', this.show.addbtn);
  }

  /**
   * Redirect user to view without page reload.
   * @param  {String} url - View/url/route to redirect the user to.
   * @return {Null}
   */
  go(url) {
   this.show.addbtn = (url === 'add') ? false : true;
   this.router.navigate(['./users/' + url]);
  }

  /**
   * Enable user edition for a particular user.
   * @param  {number} id - User's ID.
   */
  edit(id: number): void {

    // Select current user card.
    const user = document.querySelector(`#user-card-${id}`);

    // Select all form input-data in every user card.
    const allCards = document.querySelectorAll('.user-card');
    const allDisplays = document.querySelectorAll('.user-card .display-data');
    const allInputs =  document.querySelectorAll('.user-card .input-data');
    const allUpdateBtn = document.querySelectorAll('.user-card .update-btn');
    const allEditBtn = document.querySelectorAll('.user-card .option.edit');
    const allstopBtn = document.querySelectorAll('.user-card .option.stop');

    // Select current user inputs
    const display = user.getElementsByClassName('display-data');
    const inputs = user.getElementsByClassName('input-data');

    // Reset to display all the data in all the cards.
    for (let i = 0; i < allDisplays.length; i++) {
      allDisplays[i].className = 'display-data';
    }

    // Reset to hide all the edit input fields in all the cards.
    for (let i = 0; i < allInputs.length; i++) {
      allInputs[i].className = 'input-data hide';
    }

    // Reset to hide all the update user buttons in all the cards.
    for (let i = 0; i < allUpdateBtn.length; i++) {
      allUpdateBtn[i].className = 'btn btn-success update-btn hide';
    }

    // Reset to default all edit/stop buttons
    for (let i = 0; i < allEditBtn.length; i++) {
      allEditBtn[i].className = 'option edit';
      allstopBtn[i].className = 'option stop hide';
    }

    // Reset edit class in info-holder section for all the cards.
    for (let i = 0; i < allCards.length; i++) {
      allCards[i].className = 'user-card row';
    }

    // Hide all the data in the current user card only
    for (let i = 0; i < display.length; i++) {
      display[i].className = 'display-data hide';
    }

    // Select update edits button and make it visible.
    const updateBtn = document.querySelector(`#user-card-${id} .update-btn`);
    const editBtn = document.querySelectorAll(`#user-card-${id} .option.edit`);
    const stopBtn = document.querySelectorAll(`#user-card-${id} .option.stop`);
    updateBtn.className = 'btn btn-success update-btn';

    for (let i = 0; i < editBtn.length; i++) {
      editBtn[i].className = 'option edit hide';
      stopBtn[i].className = 'option stop';
    }

    user.className = 'user-card row edit';

    // Display all the edit input fields in the current user card only.
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].className = 'input-data';
    }

  }

  /**
   * Avatar image upload user data service wrapper.
   * @param  {Object} event - Upload file event.
   * @return {Null}
   */
  uploadAvatar(event: any): void{
    let fileList: FileList = event.target.files;

    if(fileList.length > 0) {
        let file: File = fileList[0];

        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        this.userData.postAvatar(formData)
                     .subscribe(avatar => this.avatars = avatar);
    }else{
      console.error("There was an error while trying to upload the avatar: " +
      "File list is empty.");
    }
  }

  /**
   * Stop and cancel edition in user's card. Modifications are not saved.
   * @param  {number} id - Id of the user's card/
   * @return {Null}
   */
  stop(id: number): void {
    // Select current user card.
    const user = document.querySelector(`#user-card-${id}`);

    // Select current user inputs
    const display = user.getElementsByClassName('display-data');
    const inputs = user.getElementsByClassName('input-data');

    // Hide all inputs in the current user card only
    for (let i = 0; i < display.length; i++) {
      inputs[i].className = 'input-data hide';
    }

    // Display all the data in the current user card only
    for (let i = 0; i < display.length; i++) {
      display[i].className = 'display-data';
    }

    // Select update edits button and hide it.
    const updateBtn = document.querySelector(`#user-card-${id} .update-btn`);
    const editBtn = document.querySelectorAll(`#user-card-${id} .option.edit`);
    const stopBtn = document.querySelectorAll(`#user-card-${id} .option.stop`);
    updateBtn.className = 'btn btn-success update-btn hide';

    for (let i = 0; i < editBtn.length; i++) {
      editBtn[i].className = 'option edit';
      stopBtn[i].className = 'option stop hide';
    }

    // Remove edit class mark in card
    user.className = 'user-card row';
  }

  /**
   * Mark the user as not active and save the changes.
   * @param  {Object} user - User's model.
   * @return {Null}
   */
  ban(user: any){
    user.isActive = false;
    user.isAdmin= false;
    this.save(user);
  }

  /**
   * Save changes to the user data.
   * @param  {Object} user - User's model to save.
   * @return {Object} - Response from the API.
   */
  save(user: any): any {
    console.log('User is: %j', user);
    const validation = this.validation(user);

    if (!validation.ok) {
      return validation;
    }

    return this.data.saveUserEdit(user);
  }

}
