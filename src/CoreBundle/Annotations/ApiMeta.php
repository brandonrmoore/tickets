<?php
namespace CoreBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class ApiMeta extends Annotation
{
    public $description;

    public function getDescription()
    {
        return $this->description;
    }

}