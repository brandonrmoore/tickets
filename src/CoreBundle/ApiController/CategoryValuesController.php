<?php
/**
 * Class CategoryValuesController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class CategoryValuesController - Controller to process routes
 *
 * Functions to process routes
 */
class CategoryValuesController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all category values that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\CategoryValues
     *
     * @ApiDoc(
     *   section = "CategoryValues",
     *   resource = true,
     *   description = "Return all category values with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[name]", "dataType" = "string", "required" = false, "description" = "Find by name."
     *       },
     *       {
     *         "name" = "findBy[category]", "dataType" = "integer", "required" = false, "description" = "Find category values by category."
     *       },
     *       {
     *         "name" = "orderBy[name]", "dataType" = "string", "required" = false, "description" = "Order results by name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[category]", "dataType" = "integer", "required" = false, "description" = "Order results by category either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category value is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/category-values")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if (!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)) {
            $limit = 1000;
        }
        if (!$offset || $offset < 0 || !is_numeric($offset)) {
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Category values not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single category value that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of category value
     * @return \CoreBundle\Entity\CategoryValues object of category
     *
     * @ApiDoc(
     *   section = "CategoryValues",
     *   resource = true,
     *   description = "Return a single category based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/category-values/{slug}")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Category Value exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new category
     *
     * @category API Controller
     * @return \CoreBundle\Entity\CategoryValues object of category
     *
     * @ApiDoc(
     *   section = "CategoryValues",
     *   resource = true,
     *   description = "Return a single category created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "category", "dataType" = "integer", "required" = true, "description" = "Id for associated category."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = true, "description" = "Name of category."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     * @JMSAnnotations\Put("/category-values")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\CategoryValues;
        if (!$paramFetcher->get('name')) {
            return new View(['error' => "The field 'name' is required."], Response::HTTP_NOT_FOUND);
        }
        if (!$paramFetcher->get('category')) {
            return new View(['error' => "The field 'category' is required."], Response::HTTP_NOT_FOUND);
        }
        $category = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Categories')->find($paramFetcher->get('category'));
        if ($category === null) {
            return new View(['error' => "No Category exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->setName($paramFetcher->get('name'));
        $restresult->setCategory($category);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Category value could not be added"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Delete category value by id
     *
     * @category API Controller
     * @param integer $slug Id of category value
     * @return array
     *
     * @ApiDoc(
     *   section = "CategoryValues",
     *   resource = true,
     *   description = "Deletes a single category value based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "DELETE" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     * @JMSAnnotations\Delete("/category-values/{slug}")
     */
    public function deleteAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Category value exists by that id"], Response::HTTP_NOT_FOUND);
        }
        try {
            $this->getDoctrine()->getManager()->remove($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Category value could not be deleted"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. CategoryValue LINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of category value
     * @return \CoreBundle\Entity\CategoryValues object of category
     *
     * @ApiDoc(
     *   section = "CategoryValues",
     *   resource = true,
     *   description = "Return a single category value updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "category", "dataType" = "integer", "required" = false, "description" = "Associated category value."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = false, "description" = "Name of category value."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     * @JMSAnnotations\Patch("/category-values/{slug}")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Category value exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if ($paramFetcher->get('name')) {
            $restresult->setName($paramFetcher->get('name'));
        }
        if ($paramFetcher->get('category')) {
            $category = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Categories')->find($paramFetcher->get('category'));
            if ($category === null) {
                return new View(['error' => "No Category exists by that id"], Response::HTTP_NOT_FOUND);
            }
            $restresult->setCategory($category);
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Category value could not be updated"], Response::HTTP_NOT_FOUND);
        }
    }
}
