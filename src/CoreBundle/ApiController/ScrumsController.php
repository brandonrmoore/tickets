<?php
/**
 * Class ScrumsController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use ElasticSearch\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class ScrumsController - Controller to process routes
 *
 * Functions to process routes
 */
class ScrumsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all users that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\Scrums
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Return all scrums with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[name]", "dataType" = "boolean", "required" = false, "description" = "Find by name."
     *       },
     *       {
     *         "name" = "orderBy[id]", "dataType" = "string", "required" = false, "description" = "Order results by id either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[name]", "dataType" = "string", "required" = false, "description" = "Order results by name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the scrum is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if(!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)){
            $limit = 1000;
        }
        if(!$offset || $offset < 0 || !is_numeric($offset)){
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Scrums not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single user that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of scrum
     * @return \CoreBundle\Entity\Scrums object of scrum
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Return a single scrum based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the scrum is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new scrum
     *
     * @category API Controller
     * @return \CoreBundle\Entity\Scrums object of scrum
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Return a single scrum created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = true, "description" = "Name of scrum."
     *       },
     *       {
     *         "name" = "scrumTimes", "dataType" = "string", "required" = false, "description" = "Scrum times in cron format."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\Groups;
        if(!$paramFetcher->get('name')) {
            return new View(['error' => "The field 'name' is required."], Response::HTTP_NOT_FOUND);
        }
        $restresult->setName($paramFetcher->get('name'));
        $restresult->setScrumTimes($paramFetcher->get('scrumTimes'));
        //scrumUsers
        if($paramFetcher->get('scrumUsers') && is_array($paramFetcher->get('scrumUsers'))){
            foreach($paramFetcher->get('scrumUsers') AS $userId){
                $user = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($userId);
                if ($user === null) {
                    return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
                }
                $restresult->addScrumUser($user);
            }

        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Scrum could not be created", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }


    }

    /**
     * Delete scrum by id
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @return array
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Deletes a single scrum based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "DELETE" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        try {
            $this->getDoctrine()->getManager()->remove($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Scrum could not be deleted", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. User LINK and UNLINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of scrum
     * @return \CoreBundle\Entity\Scrums object of user
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Return a single scrum updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = false, "description" = "First name of user."
     *       },
     *       {
     *         "name" = "scrumTimes", "dataType" = "string", "required" = false, "description" = "Scrum times in cron format."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if($paramFetcher->get('name')) {
            $restresult->setName($paramFetcher->get('username'));
        }
        if($paramFetcher->get('scrumTimes') || !$restresult->getScrumTimes()) {
            $restresult->setScrumTimes($paramFetcher->get('scrumTimes'));
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Scrum could not be updated", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a user
     *
     * @category API Controller
     * @param integer $slug Id of scrum
     * @param integer $id Id of user
     * @return \CoreBundle\Entity\Scrums object of user
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Add user relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function linkUsersAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $scrum = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($id);
        if ($scrum === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->addScrumUser($scrum);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Scrum could not be linked to user", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a user
     *
     * @category API Controller
     * @param integer $slug Id of scrum
     * @param integer $id Id of user
     * @return \CoreBundle\Entity\Scrums object of scrum
     *
     * @ApiDoc(
     *   section = "Scrums",
     *   resource = true,
     *   description = "Remove user relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function unlinkUsersAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($id);
        if ($group === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeScrumUser($group);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Scrum could not be unlinked from user", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }
}
