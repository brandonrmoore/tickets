<?php
/**
 * Class TicketAttributeTypesController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class TicketAttributeTypesController - Controller to process routes
 *
 * Functions to process routes
 */
class TicketAttributeTypesController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all category values that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\TicketAttributeTypes
     *
     * @ApiDoc(
     *   section = "TicketAttributeTypes",
     *   resource = true,
     *   description = "Return all category values with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "orderBy[name]", "dataType" = "string", "required" = false, "description" = "Order results by name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket attribute types is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-attribute-types")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if (!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)) {
            $limit = 1000;
        }
        if (!$offset || $offset < 0 || !is_numeric($offset)) {
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributeTypes')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Ticket attribute types not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single ticket workflow that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of ticket attribute type
     * @return \CoreBundle\Entity\TicketAttributeTypes object of category
     *
     * @ApiDoc(
     *   section = "TicketAttributeTypes",
     *   resource = true,
     *   description = "Return a single ticket attribute type based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket attribute type is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-attribute-types/{slug}")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributeTypes')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Attribute Type exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }
}
