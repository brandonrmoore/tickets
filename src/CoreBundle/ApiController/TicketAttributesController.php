<?php
/**
 * Class TicketAttributesController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class TicketAttributesController - Controller to process routes
 *
 * Functions to process routes
 */
class TicketAttributesController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all category values that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\TicketAttributes
     *
     * @ApiDoc(
     *   section = "TicketAttributes",
     *   resource = true,
     *   description = "Return all ticket attributes with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[ticketAttributeTypes]", "dataType" = "integer", "required" = false, "description" = "Find by ticket attribute type."
     *       },
     *       {
     *         "name" = "findBy[tickets]", "dataType" = "integer", "required" = false, "description" = "Find by ticket."
     *       },
     *       {
     *         "name" = "findBy[users]", "dataType" = "integer", "required" = false, "description" = "Find by user."
     *       },
     *       {
     *         "name" = "orderBy[dateTime]", "dataType" = "string", "required" = false, "description" = "Order results by date time 'YYYY-MM-DD H:i:s' either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[ticketAttributeTypes]", "dataType" = "integer", "required" = false, "description" = "Order results by ticket attribute types either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[users]", "dataType" = "integer", "required" = false, "description" = "Order results by users either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket attributes is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-attributes")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if (!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)) {
            $limit = 1000;
        }
        if (!$offset || $offset < 0 || !is_numeric($offset)) {
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributes')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Ticket attributes not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single ticket attribute that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of ticket attribute
     * @return \CoreBundle\Entity\TicketAttributes object of category
     *
     * @ApiDoc(
     *   section = "TicketAttributes",
     *   resource = true,
     *   description = "Return a single ticket attribute based off of the id provided in the url.",
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket attribute is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-attributes/{slug}")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributes')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Attribute exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new ticket attribute
     *
     * @category API Controller
     * @return \CoreBundle\Entity\TicketAttributes object of category
     *
     * @ApiDoc(
     *   section = "TicketAttributes",
     *   resource = true,
     *   description = "Return a single category created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "ticketAttributeTypes", "dataType" = "integer", "required" = true, "description" = "Id for associated ticket attribute type."
     *       },
     *       {
     *         "name" = "tickets", "dataType" = "integer", "required" = true, "description" = "Id for associated ticket attribute type."
     *       },
     *       {
     *         "name" = "value", "dataType" = "string", "required" = true, "description" = "Value for this ticket attribute."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Put("/ticket-attributes")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\TicketAttributes;
        /*
         * TODO Syntax checking depending on type
         */
        if (!$paramFetcher->get('value')) {
            return new View(['error' => "The field 'value' is required."], Response::HTTP_NOT_FOUND);
        }
        if (!$paramFetcher->get('ticketAttributeTypes')) {
            return new View(['error' => "The field 'ticketAttributeTypes' is required."], Response::HTTP_NOT_FOUND);
        }
        if (!$paramFetcher->get('tickets')) {
            return new View(['error' => "The field 'tickets' is required."], Response::HTTP_NOT_FOUND);
        }
        $ticketAttributeTypes = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributeTypes')->find($paramFetcher->get('ticketAttributeTypes'));
        if ($ticketAttributeTypes === null) {
            return new View(['error' => "No Ticket Attribute Type exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $tickets = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($paramFetcher->get('tickets'));
        if ($tickets === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->setValue($paramFetcher->get('value'));
        $restresult->setTickets($tickets);
        $restresult->setTicketAttributeTypes($ticketAttributeTypes);
        $restresult->setUsers($this->get('security.token_storage')->getToken()->getUser());
        $restresult->setDateTime(new \DateTime('now'));
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Category value could not be added"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Delete ticket attribute by id
     *
     * @category API Controller
     * @param integer $slug Id of ticket attribute
     * @return array
     *
     * @ApiDoc(
     *   section = "TicketAttributes",
     *   resource = true,
     *   description = "Deletes a single ticket attribute based off of the id provided in the url. Must be created by this user OR the current user is an admin",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "DELETE" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Delete("/ticket-attributes/{slug}")
     */
    public function deleteAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributes')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Attribute exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($restresult->getUsers() != $user && !$user->getIsAdmin()){
            return new View(['error' => "Must be the author or an admin to delete this attribute"], Response::HTTP_NOT_FOUND);
        }
        try {
            $this->getDoctrine()->getManager()->remove($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket attribute could not be deleted"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. Must be the author or an admin to update. TicketAttribute LINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of ticket attribute
     * @return \CoreBundle\Entity\TicketAttributes object of category
     *
     * @ApiDoc(
     *   section = "TicketAttributes",
     *   resource = true,
     *   description = "Return a single ticket attribute updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "value", "dataType" = "string", "required" = false, "description" = "Value of the ticket attribute."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Patch("/ticket-attributes/{slug}")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketAttributes')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No ticket attribute exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($restresult->getUsers() != $user && !$user->getIsAdmin()){
            return new View(['error' => "Must be the author or an admin to update this attribute"], Response::HTTP_NOT_FOUND);
        }
        if ($paramFetcher->get('value')) {
            $restresult->setValue($paramFetcher->get('value'));
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Category value could not be updated"], Response::HTTP_NOT_FOUND);
        }
    }
}
