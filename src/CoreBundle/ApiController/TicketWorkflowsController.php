<?php
/**
 * Class TicketWorkflowsController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class TicketWorkflowsController - Controller to process routes
 *
 * Functions to process routes
 */
class TicketWorkflowsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all category values that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\TicketWorkflows
     *
     * @ApiDoc(
     *   section = "TicketWorkflows",
     *   resource = true,
     *   description = "Return all category values with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[name]", "dataType" = "string", "required" = false, "description" = "Find by name."
     *       },
     *       {
     *         "name" = "findBy[enabled]", "dataType" = "boolean", "required" = false, "description" = "Workflow status is enabled or not"
     *       },
     *       {
     *         "name" = "orderBy[name]", "dataType" = "string", "required" = false, "description" = "Order results by name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[sort]", "dataType" = "integer", "required" = false, "description" = "Order results by sort value 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category value is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-workflows")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if (!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)) {
            $limit = 1000;
        }
        if (!$offset || $offset < 0 || !is_numeric($offset)) {
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketWorkflows')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Ticket workflows not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single ticket workflow that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of ticket workflow
     * @return \CoreBundle\Entity\TicketWorkflows object of category
     *
     * @ApiDoc(
     *   section = "TicketWorkflows",
     *   resource = true,
     *   description = "Return a single category based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket workflow is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Get("/ticket-workflows/{slug}")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketWorkflows')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Workflow exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new ticket workflow
     *
     * @category API Controller
     * @return \CoreBundle\Entity\TicketWorkflows object of category
     *
     * @ApiDoc(
     *   section = "TicketWorkflows",
     *   resource = true,
     *   description = "Return a single ticket workflow created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = true, "description" = "Name of ticket workflow."
     *       },
     *       {
     *         "name" = "enabled", "dataType" = "boolean", "required" = true, "description" = "Enabled or not."
     *       },
     *       {
     *         "name" = "sort", "dataType" = "smallint", "required" = true, "description" = "Sorting value."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Put("/ticket-workflows")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\TicketWorkflows;
        if (!$paramFetcher->get('name')) {
            return new View(['error' => "The field 'name' is required."], Response::HTTP_NOT_FOUND);
        }
        if (!$paramFetcher->get('sort') || $paramFetcher->get('sort') == 0) {
            return new View(['error' => "The field 'sort' is required and must be greater than 0."], Response::HTTP_NOT_FOUND);
        }
        if($paramFetcher->get('enabled') == 0 || $paramFetcher->get('enabled') == 1){
            $restresult->setEnabled($paramFetcher->get('enabled'));
        }else{
            $restresult->setEnabled(0);
        }
        $restresult->setName($paramFetcher->get('name'));
        $restresult->setSort($paramFetcher->get('sort'));
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket workflow could not be added"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. TicketWorkflows LINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of ticket workflow
     * @return \CoreBundle\Entity\TicketWorkflows object of category
     *
     * @ApiDoc(
     *   section = "TicketWorkflows",
     *   resource = true,
     *   description = "Return a single ticket workflow updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "sort", "dataType" = "integer", "required" = false, "description" = "Sort value."
     *       },
     *       {
     *         "name" = "name", "dataType" = "string", "required" = false, "description" = "Name of ticket workflow."
     *       },
     *       {
     *         "name" = "enables", "dataType" = "boolean", "required" = false, "description" = "Enable or disable ticket workflow."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Patch("/ticket-workflows/{slug}")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketWorkflows')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Workflow exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if ($paramFetcher->get('sort') == 0) {
            return new View(['error' => "The field 'sort' must be greater than 0."], Response::HTTP_NOT_FOUND);
        }
        if ($paramFetcher->get('name')) {
            $restresult->setName($paramFetcher->get('name'));
        }
        if ($paramFetcher->get('sort')) {
            $restresult->setSort($paramFetcher->get('sort'));
        }
        if($paramFetcher->get('enabled') == 0 || $paramFetcher->get('enabled') == 1){
            $restresult->setEnabled($paramFetcher->get('enabled'));
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket workflow could not be updated","message" => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }
}
