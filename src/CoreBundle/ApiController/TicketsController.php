<?php
/**
 * Class TicketsController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class TicketAttributesController - Controller to process routes
 *
 * Functions to process routes
 */
class TicketsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all tickets that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\Tickets
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Return all ticket attributes with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[ticketWorkflows]", "dataType" = "integer", "required" = false, "description" = "Find by ticket workflow."
     *       },
     *       {
     *         "name" = "findBy[ticketCategoryValues]", "dataType" = "integer", "required" = false, "description" = "Find by ticket category value."
     *       },
     *       {
     *         "name" = "findBy[ticketUsers]", "dataType" = "integer", "required" = false, "description" = "Find by user."
     *       },
     *       {
     *         "name" = "findBy[ticketGroups]", "dataType" = "integer", "required" = false, "description" = "Find by group."
     *       },
     *       {
     *         "name" = "orderBy[creationDateTime]", "dataType" = "string", "required" = false, "description" = "Order results by creation date time either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[dueDateTime]", "dataType" = "string", "required" = false, "description" = "Order results by due date time either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[ticketWorkflows]", "dataType" = "integer", "required" = false, "description" = "Order results by ticket workflows either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[ticketUsers]", "dataType" = "integer", "required" = false, "description" = "Order results by users either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[ticketGroups]", "dataType" = "integer", "required" = false, "description" = "Order results by groups either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 100"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket attributes is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 100;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if (!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)) {
            $limit = 1000;
        }
        if (!$offset || $offset < 0 || !is_numeric($offset)) {
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Tickets not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single ticket that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Return a single ticket based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new ticket
     *
     * @category API Controller
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Return a single ticket created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "sort", "dataType" = "smallint", "required" = true, "description" = "sort order for ticket."
     *       },
     *       {
     *         "name" = "secondsEstimate", "dataType" = "integer", "required" = true, "description" = "Id for associated ticket attribute type."
     *       },
     *       {
     *         "name" = "dueDateTime", "dataType" = "string", "required" = true, "description" = "Due date in 'YYYY-MM-DD H:i:s' format."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\Tickets;

        $ticketWorkflows = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketWorkflows')->findOneBy(
            ['enabled' => 1],
            ['sort' => 'ASC']
        );
        if ($ticketWorkflows === null) {
            return new View(['error' => "Could not find a default ticket workflow"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $restresult->setTicketWorkflows($ticketWorkflows);
        $restresult->setUsers($user);
        if($paramFetcher->get('sort')){
            $restresult->setSort($paramFetcher->get('sort'));
        }else{
            $restresult->setSort(9999);
        }
        if($paramFetcher->get('secondsEstimate')){
            $restresult->setSort($paramFetcher->get('secondsEstimate'));
        }else{
            $restresult->setSort(0);
        }
        if($paramFetcher->get('dueDateTime')){
            $restresult->setDueDateTime(new \DateTime($paramFetcher->get('dueDateTime')));
        }else{
            $restresult->setDueDateTime(null);
        }
        $restresult->setCreationDateTime(new \DateTime('now'));
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket could not be added","message" => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Delete ticket attribute by id
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @return array
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Deletes a single ticket based off of the id provided in the url. Must be created by this user OR the current user is an admin",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "DELETE" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the category is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function deleteAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket Attribute exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($restresult->getUsers() != $user && !$user->getIsAdmin()){
            return new View(['error' => "Must be the creator or an admin to delete this ticket"], Response::HTTP_NOT_FOUND);
        }
        try {
            $this->getDoctrine()->getManager()->remove($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket could not be deleted"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. Tickets LINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of ticket attribute
     * @return \CoreBundle\Entity\TicketAttributes object of category
     *
     * @ApiDoc(
     *   section = "Ticket",
     *   resource = true,
     *   description = "Return a single ticket attribute updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "sort", "dataType" = "smallint", "required" = true, "description" = "sort order for ticket."
     *       },
     *       {
     *         "name" = "secondsEstimate", "dataType" = "integer", "required" = true, "description" = "Estimate seconds to complete task."
     *       },
     *       {
     *         "name" = "secondsLogged", "dataType" = "integer", "required" = true, "description" = "Seconds spent on task."
     *       },
     *       {
     *         "name" = "dueDateTime", "dataType" = "string", "required" = true, "description" = "Due date in 'YYYY-MM-DD H:i:s' format."
     *       },
     *       {
     *         "name" = "ticketWorkflows", "dataType" = "integer", "required" = true, "description" = "Id for associated ticket workflow."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if ($paramFetcher->get('ticketWorkflows')) {
            $ticketWorkflows = $this->getDoctrine()->getManager()->getRepository('CoreBundle:TicketWorkflows')->find($paramFetcher->get('ticketWorkflows'));
            if ($ticketWorkflows === null) {
                return new View(['error' => "Ticket workflow was specified, but one does not exist by provided id"], Response::HTTP_NOT_FOUND);
            }
            $restresult->setTicketWorkflows($ticketWorkflows);
        }
        if ($paramFetcher->get('sort')) {
            $restresult->setSort($paramFetcher->get('sort'));
        }
        if ($paramFetcher->get('secondsEstimate') >= 0) {
            $restresult->setSecondsEstimate($paramFetcher->get('secondsEstimate'));
        }
        if ($paramFetcher->get('secondsLogged') >= 0) {
            $restresult->setSecondsLogged($paramFetcher->get('secondsLogged'));
        }
        if ($paramFetcher->get('dueDateTime')) {
            $restresult->setDueDateTime(new \DateTime($paramFetcher->get('dueDateTime')));
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch (\Exception $e) {
            return new View(['error' => "Ticket could not be updated"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a user
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of user
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Assign ticket to user",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function linkUsersAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($id);
        if ($user === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        //remove existing users
        foreach($restresult->getTicketUsers() AS $removeUser){
            $this->unlinkUsersAction($slug,$removeUser->getId());
        }
        //remove existing groups
        foreach($restresult->getTicketGroups() AS $removeGroup){
            $this->unlinkGroupsAction($slug,$removeGroup->getId());
        }
        //add new user
        $restresult->addTicketUser($user);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be linked to ticket", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a user
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of user
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Unassign ticket from user",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function unlinkUsersAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $user = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($id);
        if ($user === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeTicketUser($user);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Ticket could not be unlinked from user", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a group
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of group
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Assign ticket to group",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function linkGroupsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($id);
        if ($group === null) {
            return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
        }
        //remove existing users
        foreach($restresult->getTicketUsers() AS $removeUser){
            $this->unlinkUsersAction($slug,$removeUser->getId());
        }
        //remove existing groups
        foreach($restresult->getTicketGroups() AS $removeGroup){
            $this->unlinkGroupsAction($slug,$removeGroup->getId());
        }
        //add new group
        $restresult->addTicketGroup($group);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Ticket could not be linked to group", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a group
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of group
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Unassign ticket from group",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     */
    public function unlinkGroupsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($id);
        if ($group === null) {
            return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeTicketGroup($group);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Ticket could not be unlinked from group", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a category value
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of category value
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Assign ticket to a category value",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Link("/tickets/{slug}/category-values/{id}")
     */
    public function linkCategoryValuesAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $categoryValue = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->find($id);
        if ($categoryValue === null) {
            return new View(['error' => "No Category Value exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->addTicketCategoryValue($categoryValue);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Category Value could not be linked to ticket", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a category value
     *
     * @category API Controller
     * @param integer $slug Id of ticket
     * @param integer $id Id of category value
     * @return \CoreBundle\Entity\Tickets object of ticket
     *
     * @ApiDoc(
     *   section = "Tickets",
     *   resource = true,
     *   description = "Unassign ticket from category value",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the ticket is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\Unlink("/tickets/{slug}/category-values/{id}")
     */
    public function unlinkCategoryValuesAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Tickets')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No Ticket exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $categoryValue = $this->getDoctrine()->getManager()->getRepository('CoreBundle:CategoryValues')->find($id);
        if ($categoryValue === null) {
            return new View(['error' => "No Category Value exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeTicketCategoryValue($categoryValue);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "Ticket could not be unlinked from category value", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }
}
