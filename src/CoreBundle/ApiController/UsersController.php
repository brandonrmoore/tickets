<?php
/**
 * Class UsersController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\ApiController;

use CoreBundle\CoreBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations AS JMSAnnotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class UsersController - Controller to process routes
 *
 * Functions to process routes
 */
class UsersController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Get all users that match supplied parameters
     *
     * @category API Controller
     *
     * @return array Array of CoreBundle\Entity\Users
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Return all users with a default limit of 1000 and offset of 0.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "findBy[isActive]", "dataType" = "boolean", "required" = false, "description" = "Find active or inactive users."
     *       },
     *       {
     *         "name" = "findBy[isAdmin]", "dataType" = "boolean", "required" = false, "description" = "Find admin or non-admin users."
     *       },
     *       {
     *         "name" = "findBy[email]", "dataType" = "string", "required" = false, "description" = "Find by email."
     *       },
     *       {
     *         "name" = "findBy[firstName]", "dataType" = "string", "required" = false, "description" = "Find by first name."
     *       },
     *       {
     *         "name" = "findBy[lastName]", "dataType" = "string", "required" = false, "description" = "Find by last name."
     *       },
     *       {
     *         "name" = "orderBy[isActive]", "dataType" = "string", "required" = false, "description" = "Order results by isActive either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[isAdmin]", "dataType" = "string", "required" = false, "description" = "Order results by isAdmin either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[email]", "dataType" = "string", "required" = false, "description" = "Order results by email either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[firstName]", "dataType" = "string", "required" = false, "description" = "Order results by first name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "orderBy[lastName]", "dataType" = "string", "required" = false, "description" = "Order results by last name either 'ASC' or 'DESC'."
     *       },
     *       {
     *         "name" = "limit", "dataType" = "integer", "required" = false, "description" = "Limit results. Maximum of 1000"
     *       },
     *       {
     *         "name" = "offset", "dataType" = "integer", "required" = false, "description" = "Offset of returned results."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function cgetAction()
    {
        $findBy = $this->container->get('request_stack')->getCurrentRequest()->get('findBy') ?: [];
        $orderBy = $this->container->get('request_stack')->getCurrentRequest()->get('orderBy') ?: [];
        $limit = $this->container->get('request_stack')->getCurrentRequest()->get('limit') ?: 1000;
        $offset = $this->container->get('request_stack')->getCurrentRequest()->get('offset') ?: 0;
        if(!$limit || $limit > 1000 || $limit < 0 || !is_numeric($limit)){
            $limit = 1000;
        }
        if(!$offset || $offset < 0 || !is_numeric($offset)){
            $offset = 0;
        }
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->findBy(
            $findBy,
            $orderBy,
            $limit,
            $offset
        );
        if ($restresult === null) {
            return new View(['error' => "Users not found"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Get a single user that match the id provided in the url
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Return a single user based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "GET" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_USER,ROLE_ADMIN")
     * @JMSAnnotations\View(serializerEnableMaxDepthChecks=true)
     */
    public function getAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Create a new user
     *
     * @category API Controller
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Return a single user created from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "isActive", "dataType" = "boolean", "required" = true, "description" = "Active status of user."
     *       },
     *       {
     *         "name" = "isAdmin", "dataType" = "boolean", "required" = true, "description" = "Admin status of user."
     *       },
     *       {
     *         "name" = "email", "dataType" = "string", "required" = true, "description" = "Email of user."
     *       },
     *       {
     *         "name" = "firstName", "dataType" = "string", "required" = true, "description" = "First name of user."
     *       },
     *       {
     *         "name" = "lastName", "dataType" = "string", "required" = true, "description" = "Last name of user."
     *       },
     *       {
     *         "name" = "password", "dataType" = "string", "required" = true, "description" = "Password of user."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PUT" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function putAction()
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = new \CoreBundle\Entity\Users;
        if(!$paramFetcher->get('username')) {
            return new View(['error' => "The field 'username' is required."], Response::HTTP_NOT_FOUND);
        }
        if(!$paramFetcher->get('email')){
            return new View(['error' => "The field 'email' is required."], Response::HTTP_NOT_FOUND);
        }
        if(!$paramFetcher->get('password')){
            return new View(['error' => "The field 'password' is required."], Response::HTTP_NOT_FOUND);
        }
        if(!$paramFetcher->get('firstName')){
            return new View(['error' => "The field 'firstName' is required."], Response::HTTP_NOT_FOUND);
        }
        if(!$paramFetcher->get('lastName')){
            return new View(['error' => "The field 'lastName' is required."], Response::HTTP_NOT_FOUND);
        }
        $restresult->setUsername($paramFetcher->get('username'));
        $restresult->setFirstName($paramFetcher->get('firstName'));
        $restresult->setLastname($paramFetcher->get('lastName'));
        $restresult->setEmail($paramFetcher->get('email'));

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($restresult, $paramFetcher->get('password'));
        $restresult->setPassword($password);

        $restresult->setIsActive(1);
        if($paramFetcher->get('isActive')){
            $restresult->setIsActive($paramFetcher->get('isActive'));
        }
        $restresult->setIsAdmin(0);
        if($paramFetcher->get('isAdmin')){
            $restresult->setIsAdmin($paramFetcher->get('lastName'));
        }
        //userGroups
        if($paramFetcher->get('userGroups') && is_array($paramFetcher->get('userGroups'))){
            foreach($paramFetcher->get('userGroups') AS $groupId){
                $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($groupId);
                if ($group === null) {
                    return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
                }
                $restresult->addUserGroup($group);
            }

        }
        //userScrums
        if($paramFetcher->get('userScrums') && is_array($paramFetcher->get('userScrums'))){
            foreach($paramFetcher->get('userScrums') AS $scrumId){
                $scrum = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($scrumId);
                if ($scrum === null) {
                    return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
                }
                $restresult->addUserScrum($scrum);
            }

        }
        $this->getDoctrine()->getManager()->persist($restresult);
        $this->getDoctrine()->getManager()->flush();
        return $restresult;
    }

    /**
     * Delete user by id
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @return array
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Deletes a single user based off of the id provided in the url.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "DELETE" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        try {
            $restresult->setIsActive(0);
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be deleted", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update entity. Only updates direct fields. User LINK methods for relationships
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Return a single user updated from provided parameters.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "isActive", "dataType" = "boolean", "required" = false, "description" = "Active status of user."
     *       },
     *       {
     *         "name" = "isAdmin", "dataType" = "boolean", "required" = false, "description" = "Admin status of user."
     *       },
     *       {
     *         "name" = "email", "dataType" = "string", "required" = false, "description" = "Email of user."
     *       },
     *       {
     *         "name" = "firstName", "dataType" = "string", "required" = false, "description" = "First name of user."
     *       },
     *       {
     *         "name" = "lastName", "dataType" = "string", "required" = false, "description" = "Last name of user."
     *       },
     *       {
     *         "name" = "password", "dataType" = "string", "required" = false, "description" = "Password of user."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "PATCH" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function patchAction($slug)
    {
        $paramFetcher = $this->container->get('request_stack')->getCurrentRequest();
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if($paramFetcher->get('username')) {
            $restresult->setUsername($paramFetcher->get('username'));
        }
        if($paramFetcher->get('email')){
            $restresult->setEmail($paramFetcher->get('email'));
        }
        if($paramFetcher->get('password')){
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($restresult, $paramFetcher->get('password'));
            $restresult->setPassword($password);
        }
        if($paramFetcher->get('firstName')){
            $restresult->setFirstName($paramFetcher->get('firstName'));
        }
        if($paramFetcher->get('lastName')){
            $restresult->setLastname($paramFetcher->get('lastName'));
        }
        if($paramFetcher->get('isActive') !== null){
            $restresult->setIsActive($paramFetcher->get('isActive'));
        }
        if($paramFetcher->get('isAdmin') !== null){
            $restresult->setIsAdmin($paramFetcher->get('lastName'));
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be updated", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Upload avatar image.
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Upload an avatar for the user.",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *       {
     *         "name" = "avatar", "dataType" = "file", "required" = true, "description" = "Avatar image to upload."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "POST" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned if parameters are missing"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function postAvatarAction($slug)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        if(isset($_FILES['avatar'])){
            $uploaderService = $this->container->get('attachment.uploader');
            if($restresult->getAvatar()){
                $uploaderService->delete($restresult->getAvatar());
            }
            $fileName = $uploaderService->upload($_FILES['avatar']);
            $restresult->setAvatar($fileName);
        }
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User avatar not be added", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a group
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @param integer $id Id of group
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Add group relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function linkGroupsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($id);
        if ($group === null) {
            return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->addUserGroup($group);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be linked to group", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a group
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @param integer $id Id of group
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Remove group relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function unlinkGroupsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $group = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Groups')->find($id);
        if ($group === null) {
            return new View(['error' => "No Group exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeUserGroup($group);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be unlinked from group", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add relationship to a scrum
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @param integer $id Id of scrum
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Add scrum relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "LINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function linkScrumsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $scrum = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($id);
        if ($scrum === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->addUserScrum($scrum);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be linked to scrum", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove relationship to a scrum
     *
     * @category API Controller
     * @param integer $slug Id of user
     * @param integer $id Id of scrum
     * @return \CoreBundle\Entity\Users object of user
     *
     * @ApiDoc(
     *   section = "Users",
     *   resource = true,
     *   description = "Remove scrum relationship",
     *   parameters = {
     *       {
     *         "name" = "_method", "dataType" = "string", "required" = false, "description" = "Overrides the Method supplied in the HTTP header."
     *       },
     *   },
     *   headers = {
     *       { "name" = "METHOD", "required" = true, "description" = "UNLINK" }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     * @Secure(roles="ROLE_ADMIN")
     */
    public function unlinkScrumsAction($slug,$id)
    {
        $restresult = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Users')->find($slug);
        if ($restresult === null) {
            return new View(['error' => "No User exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $scrum = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Scrums')->find($id);
        if ($scrum === null) {
            return new View(['error' => "No Scrum exists by that id"], Response::HTTP_NOT_FOUND);
        }
        $restresult->removeUserScrum($scrum);
        try {
            $this->getDoctrine()->getManager()->persist($restresult);
            $this->getDoctrine()->getManager()->flush();
            return $restresult;
        } catch(\Exception $e) {
            return new View(['error' => "User could not be unlinked from scrum", 'message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }
}
