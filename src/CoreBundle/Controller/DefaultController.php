<?php
/**
 * Class DefaultController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController - Controller to process routes
 *
 * Functions to process routes
 */
class DefaultController extends Controller
{

    /**
     * This is a catch all response to return the Angular UI.
     *
     * The UI is driven by a JavaScript frontend, Angular 2. FOr all routes in Angular there should be a corresponding
     * Symfony route that comes to this action. This will ensure that Angular 2 is able to render the UI and from there
     * Angular can make the AJAX calls needed to grab data for the requested page.
     *
     * @category Controller
     * @param \Symfony\Component\HttpFoundation\Request $request This is the default Symfony Request object
     *
     * @return string The HTML rendered from the Angular template
     */
    public function indexAction(Request $request)
    {
        return $this->render('CoreBundle:default:index.html.twig');
    }

    /**
     * Location for Documentation for API v1
     *
     * This simply loads a static file and outputs as JSON.
     *
     * @category Controller
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function docsApiV1Action()
    {
        $data = [];
        $file = $this->get('kernel')->getRootDir().'/../api_docs/v1.json';
        if(file_exists($file)){
            $data = json_decode(file_get_contents($file));
        }
        return new JsonResponse($data);
    }

    /**
     * This is the function for downloading attachments
     *
     * Attachments are stored in the /uploads directory outside of the webroot. It is done this way for added security
     * in ensuring a user must be authenticated before they can download attachments
     *
     * @category Controller
     * @param string $fileName this is the md5 file name
     * @param string $downloadedName this is the desired download name of the file
     *
     * @return \Symfony\Component\HttpFoundation\Response Response object to download the file
     */
    public function attachmentsAction($fileName,$downloadedName)
    {
        $attachmentUploader = $this->get('attachment.uploader');
        $fileFullPath = $attachmentUploader->getPath($fileName);
        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($fileFullPath));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($downloadedName) . '";');
        $response->headers->set('Content-length', filesize($fileFullPath));

        $response->setContent(file_get_contents($fileFullPath));
        return $response;
    }

    /**
     * This is the function for displaying avatars
     *
     * Avatars are stored in the /uploads directory outside of the webroot. It is done this way for added security
     * in ensuring a user must be authenticated before they can see avatars
     *
     * @category Controller
     * @param string $fileName this is the md5 file name
     *
     * @return \Symfony\Component\HttpFoundation\Response Response object to view the file
     */
    public function avatarsAction($fileName)
    {
        $attachmentUploader = $this->get('attachment.uploader');
        $fileFullPath = $attachmentUploader->getPath($fileName);
        if(!getimagesize($fileFullPath)){
            throw $this->createNotFoundException('Avatar not found');
        }
        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($fileFullPath));
        $response->headers->set('Content-length', filesize($fileFullPath));

        $response->setContent(file_get_contents($fileFullPath));
        return $response;
    }
}
