<?php
/**
 * Class SecurityController
 *
 * @package     CoreBundle
 * @subpackage  Controller
 */
namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
 * Class SecurityController - Controller to process authentication routes
 *
 * Functions to process authentication routes
 */
class SecurityController extends Controller
{
    /**
     * Handle logins.
     *
     * Authenticate users and display the login page
     *
     * @category Authentication Controller
     * @param \Symfony\Component\HttpFoundation\Request $request This is the default Symfony Request object
     *
     * @return string The HTML rendered from the Angular template
     */
    public function loginAction(Request $request)
    {
        $path = $request->getPathInfo();
        $arr = explode('/',$path);
        $type = $arr[1];
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
    
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'CoreBundle:login:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
                'type'          => $type
            )
        );
    }

    /**
     * Dummy route action.
     *
     * Checks for authentication.
     *
     * @category Authentication Controller
     *
     * @return string The HTML rendered from the Angular template
     */
    public function loginCheckAction()
    {
        die('here');
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
}
