<?php
/**
 * Class CoreBundle
 *
 * @package CoreBundle
 */
namespace CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * CoreBundle
 */
class CoreBundle extends Bundle
{
}
