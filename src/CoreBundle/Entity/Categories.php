<?php
/**
 * Class Categories
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */

namespace CoreBundle\Entity;

use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * Categories - Entity for Categories
 *
 */
class Categories
{
    /**
     * @var string category name
     * @ApiMeta(description="The name of the Category")
     */
    private $name;

    /**
     * @var boolean allows multiple to be selected for a ticket
     * @ApiMeta(description="This category allows multiple values to be set")
     */
    private $multiple;

    /**
     * @var integer unique ID field
     * @ApiMeta(description="Primary key")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection array of values for this category
     * @ApiMeta(description="CategoryValues for this Category")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $categoryValues;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categoryValues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name set the name
     *
     * @return Categories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name get the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple true or false as to whether multiple values are allowed
     *
     * @return Categories
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add categoryValue
     *
     * @param \CoreBundle\Entity\CategoryValues $categoryValue associated value for this category
     *
     * @return Categories
     */
    public function addCategoryValue(\CoreBundle\Entity\CategoryValues $categoryValue)
    {
        if (!$this->categoryValues || !$this->categoryValues->contains($categoryValue)) {
            $this->categoryValues[] = $categoryValue;
            $categoryValue->setCategory($this);
        }
        return $this;
    }

    /**
     * Remove categoryValue
     *
     * @param \CoreBundle\Entity\CategoryValues $categoryValue associated values for this category
     */
    public function removeCategoryValue(\CoreBundle\Entity\CategoryValues $categoryValue)
    {
        $this->categoryValues->removeElement($categoryValue);
    }

    /**
     * Get categoryValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategoryValues()
    {
        return $this->categoryValues;
    }

    /**
     * Get category values as an array
     *
     * @return array
     */
    public function getCategoryValuesArray()
    {
        $ids = [];
        foreach($this->categoryValues AS $value){
            $ids[] = $value->getId();
        }
        return $ids;
    }
}
