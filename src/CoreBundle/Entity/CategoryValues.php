<?php
/**
 * Class CategorieValues
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * CategoryValues
 */
class CategoryValues
{
    /**
     * @var string value name
     * @ApiMeta(description="The name of the CategoryValue")
     */
    private $name;

    /**
     * @var integer unique ID
     * @ApiMeta(description="Primary key")
     */
    private $id;

    /**
     * @var \CoreBundle\Entity\Categories associated category
     * @ApiMeta(description="Associated Category")
     * @JMSAnnotation\MaxDepth(1)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $category;


    /**
     * Set name
     *
     * @param string $name value name to set
     *
     * @return CategoryValues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \CoreBundle\Entity\Categories $category associated category to set
     *
     * @return CategoryValues
     */
    public function setCategory(\CoreBundle\Entity\Categories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \CoreBundle\Entity\Categories
     */
    public function getCategory()
    {
        return $this->category;
    }
}
