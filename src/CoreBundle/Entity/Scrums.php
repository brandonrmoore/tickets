<?php
/**
 * Class Scrums
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * Scrums
 */
class Scrums
{
    /**
     * @var string scrum name
     * @ApiMeta(description="The name of the Scrum")
     */
    private $name;

    /**
     * @var string scrum times in cron format
     * @ApiMeta(description="The schedule for this Scrum in cron format")
     */
    private $scrumTimes;

    /**
     * @var integer unique ID
     * @ApiMeta(description="Primary key")
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name name to set
     *
     * @return Scrums
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set scrumTimes
     *
     * @param \string $scrumTimes times in json format for scrum
     *
     * @return Scrums
     */
    public function setScrumTimes($scrumTimes)
    {
        json_decode($scrumTimes);
        if(json_last_error() == JSON_ERROR_NONE){
            $scrumTimes = json_encode([]);
        }
        $this->scrumTimes = $scrumTimes;

        return $this;
    }

    /**
     * Get scrumTimes
     *
     * @return \string
     */
    public function getScrumTimes()
    {
        return json_decode($this->scrumTimes);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection array of users for this scrum
     * @ApiMeta(description="Users associated with this Scrum")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $scrumUsers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scrumUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add scrumUser
     *
     * @param \CoreBundle\Entity\Users $scrumUser user to add to this scrum
     *
     * @return Scrums
     */
    public function addScrumUser(\CoreBundle\Entity\Users $scrumUser)
    {
        $this->scrumUsers[] = $scrumUser;

        return $this;
    }

    /**
     * Remove scrumUser
     *
     * @param \CoreBundle\Entity\Users $scrumUser user to remove from this scrum
     */
    public function removeScrumUser(\CoreBundle\Entity\Users $scrumUser)
    {
        $this->scrumUsers->removeElement($scrumUser);
    }

    /**
     * Get scrumUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScrumUsers()
    {
        return $this->scrumUsers;
    }

    /**
     * Get scrumUsers as array
     *
     * @return array
     */
    public function getScrumUsersArray()
    {
        $ids = [];
        foreach($this->scrumUsers AS $user){
            $ids[] = $user->getId();
        }
        return $ids;
    }
}
