<?php
/**
 * Class TicketAttributeTypes
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * TicketAttributeTypes
 */
class TicketAttributeTypes
{
    /**
     * @var string name of type
     * @ApiMeta(description="The name of this TicketAttributeType")
     */
    private $name;

    /**
     * @var integer primary key
     * @ApiMeta(description="Primary key")
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name name to set
     *
     * @return TicketAttributeTypes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection ticket attributes of this type
     * @ApiMeta(description="TicketAttributes associated with this TicketAttributeType")
     * @JMSAnnotation\Exclude()
     * @JMSAnnotation\MaxDepth(0)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketAttributes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketAttributes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute attribute to add
     *
     * @return TicketAttributeTypes
     */
    public function addTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes[] = $ticketAttribute;

        return $this;
    }

    /**
     * Remove ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute attribute to remove
     */
    public function removeTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes->removeElement($ticketAttribute);
    }

    /**
     * Get ticketAttributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketAttributes()
    {
        return $this->ticketAttributes;
    }
}
