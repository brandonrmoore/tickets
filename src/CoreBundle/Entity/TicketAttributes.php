<?php
/**
 * Class TicketAttributes
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */

namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * TicketAttributes
 */
class TicketAttributes
{

    /**
     * @var string value
     * @ApiMeta(description="The value of this TicketAttribute")
     */
    private $value;

    /**
     * @var \DateTime time of this attribute
     * @ApiMeta(description="The timestamp of this TicketAttribute")
     */
    private $dateTime;

    /**
     * @var integer unique id
     * @ApiMeta(description="Primary key")
     */
    private $id;

    /**
     * Set value
     *
     * @param string $value set the value
     *
     * @return TicketAttributes
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime set the datetime of this attribute
     *
     * @return TicketAttributes
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \CoreBundle\Entity\Tickets Ticket entity
     * @ApiMeta(description="Ticket associated with this TicketAttribute")
     * @JMSAnnotation\MaxDepth(1)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $tickets;

    /**
     * @var \CoreBundle\Entity\Users User entity
     * @ApiMeta(description="User associated with this TicketAttribute")
     * @JMSAnnotation\MaxDepth(1)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $users;


    /**
     * Set tickets
     *
     * @param \CoreBundle\Entity\Tickets $tickets set the ticket for this attribue
     *
     * @return TicketAttributes
     */
    public function setTickets(\CoreBundle\Entity\Tickets $tickets = null)
    {
        $this->tickets = $tickets;

        return $this;
    }

    /**
     * Get tickets
     *
     * @return \CoreBundle\Entity\Tickets
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Set users
     *
     * @param \CoreBundle\Entity\Users $users set the user for this attribute
     *
     * @return TicketAttributes
     */
    public function setUsers(\CoreBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \CoreBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * @var \CoreBundle\Entity\TicketAttributeTypes ticket attribute type such as comment or title
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketAttributeTypes;


    /**
     * Set ticketAttributeTypes
     *
     * @param \CoreBundle\Entity\TicketAttributeTypes $ticketAttributeTypes
     *
     * @return TicketAttributes
     */
    public function setTicketAttributeTypes(\CoreBundle\Entity\TicketAttributeTypes $ticketAttributeTypes = null)
    {
        $this->ticketAttributeTypes = $ticketAttributeTypes;

        return $this;
    }

    /**
     * Get ticketAttributeTypes
     *
     * @return \CoreBundle\Entity\TicketAttributeTypes
     */
    public function getTicketAttributeTypes()
    {
        return $this->ticketAttributeTypes;
    }
}
