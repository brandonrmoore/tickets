<?php
/**
 * Class TicketWorkflows
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * TicketWorkflows
 */
class TicketWorkflows
{
    /**
     * @var string workflow name
     * @ApiMeta(description="The name of this TicketWorkflow")
     */
    private $name;

    /**
     * @var boolean is this enabled
     * @ApiMeta(description="The enabled status of this TicketWorkflow")
     */
    private $enabled = true;

    /**
     * @var integer sort order
     * @ApiMeta(description="The sorting order of this TicketWorkflow")
     */
    private $sort;

    /**
     * @var integer unique ID
     * @ApiMeta(description="Primary key")
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name name to set
     *
     * @return TicketWorkflows
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled set if enabled or not
     *
     * @return TicketWorkflows
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set sort
     *
     * @param integer $sort set sorted value
     *
     * @return TicketWorkflows
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection collections of tickets for this workflow status
     * @ApiMeta(description="Tickets associated with this TicketWorkflow")
     * @JMSAnnotation\Exclude()
     * @JMSAnnotation\MaxDepth(0)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketWorkflowTickets;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketWorkflowTickets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticketWorkflowTicket
     *
     * @param \CoreBundle\Entity\Tickets $ticketWorkflowTicket entity of Tickets to add
     *
     * @return TicketWorkflows
     */
    public function addTicketWorkflowTicket(\CoreBundle\Entity\Tickets $ticketWorkflowTicket)
    {
        $this->ticketWorkflowTickets[] = $ticketWorkflowTicket;

        return $this;
    }

    /**
     * Remove ticketWorkflowTicket
     *
     * @param \CoreBundle\Entity\Tickets $ticketWorkflowTicket entity of Tickets to remove
     */
    public function removeTicketWorkflowTicket(\CoreBundle\Entity\Tickets $ticketWorkflowTicket)
    {
        $this->ticketWorkflowTickets->removeElement($ticketWorkflowTicket);
    }

    /**
     * Get ticketWorkflowTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketWorkflowTickets()
    {
        return $this->ticketWorkflowTickets;
    }
}
