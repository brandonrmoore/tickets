<?php
/**
 * Class Tickets
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * Tickets
 */
class Tickets
{
    /**
     * @var integer how this ticket is sorted for user
     * @ApiMeta(description="The sorting order of this Ticket")
     */
    private $sort = '1';

    /**
     * @var integer number of seconds logged for this ticket
     * @ApiMeta(description="Seconds of work logged for this Ticket")
     */
    private $secondsLogged = '0';

    /**
     * @var integer unique id
     * @ApiMeta(description="Primary key")
     */
    private $id;


    /**
     * Set sort
     *
     * @param integer $sort set the sorting value
     *
     * @return Tickets
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set secondsLogged
     *
     * @param integer $secondsLogged set the seconds logged
     *
     * @return Tickets
     */
    public function setSecondsLogged($secondsLogged)
    {
        $this->secondsLogged = $secondsLogged;

        return $this;
    }

    /**
     * Get secondsLogged
     *
     * @return integer
     */
    public function getSecondsLogged()
    {
        return $this->secondsLogged;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection associated TicketAttributes
     * @ApiMeta(description="TicketAttributes for this Ticket")
     * @JMSAnnotation\MaxDepth(3)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketAttributes;

    /**
     * @var \CoreBundle\Entity\TicketWorkflows associated TicketWorkflows
     * @ApiMeta(description="TicketWorkflow for this Ticket")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketWorkflows;

    /**
     * @var \Doctrine\Common\Collections\Collection associated TicketUsers
     * @ApiMeta(description="Users for this Ticket")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketUsers;

    /**
     * @var \Doctrine\Common\Collections\Collection associated TicketGroups
     * @ApiMeta(description="Groups for this Ticket")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketGroups;

    /**
     * @var \Doctrine\Common\Collections\Collection associated TicketCategoryValues
     * @ApiMeta(description="CategoryValues for this Ticket")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketCategoryValues;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketAttributes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ticketUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ticketGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ticketCategoryValues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute add associated TicketAttribute
     *
     * @return Tickets
     */
    public function addTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes[] = $ticketAttribute;

        return $this;
    }

    /**
     * Remove ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute remove associated TicketAttribute
     */
    public function removeTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes->removeElement($ticketAttribute);
    }

    /**
     * Get ticketAttributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketAttributes()
    {
        return $this->ticketAttributes;
    }

    /**
     * Set ticketWorkflows
     *
     * @param \CoreBundle\Entity\TicketWorkflows $ticketWorkflows set associated TicketWorkflow
     *
     * @return Tickets
     */
    public function setTicketWorkflows(\CoreBundle\Entity\TicketWorkflows $ticketWorkflows = null)
    {
        $this->ticketWorkflows = $ticketWorkflows;

        return $this;
    }

    /**
     * Get ticketWorkflows
     *
     * @return \CoreBundle\Entity\TicketWorkflows
     */
    public function getTicketWorkflows()
    {
        return $this->ticketWorkflows;
    }

    /**
     * Add ticketUser
     *
     * @param \CoreBundle\Entity\Users $ticketUser add associated User
     *
     * @return Tickets
     */
    public function addTicketUser(\CoreBundle\Entity\Users $ticketUser)
    {
        $this->ticketUsers[] = $ticketUser;

        return $this;
    }

    /**
     * Remove ticketUser
     *
     * @param \CoreBundle\Entity\Users $ticketUser remove associated User
     */
    public function removeTicketUser(\CoreBundle\Entity\Users $ticketUser)
    {
        $this->ticketUsers->removeElement($ticketUser);
    }

    /**
     * Get ticketUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketUsers()
    {
        return $this->ticketUsers;
    }

    /**
     * Add ticketGroup
     *
     * @param \CoreBundle\Entity\Groups $ticketGroup add associated TicketGroup
     *
     * @return Tickets
     */
    public function addTicketGroup(\CoreBundle\Entity\Groups $ticketGroup)
    {
        $this->ticketGroups[] = $ticketGroup;

        return $this;
    }

    /**
     * Remove ticketGroup
     *
     * @param \CoreBundle\Entity\Groups $ticketGroup add associated TicketGroup
     */
    public function removeTicketGroup(\CoreBundle\Entity\Groups $ticketGroup)
    {
        $this->ticketGroups->removeElement($ticketGroup);
    }

    /**
     * Get ticketGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketGroups()
    {
        return $this->ticketGroups;
    }

    /**
     * Add ticketCategoryValue
     *
     * @param \CoreBundle\Entity\CategoryValues $ticketCategoryValue CategoryValues to add
     *
     * @return Tickets
     */
    public function addTicketCategoryValue(\CoreBundle\Entity\CategoryValues $ticketCategoryValue)
    {
        if (!$this->ticketCategoryValues || !$this->ticketCategoryValues->contains($ticketCategoryValue)) {
            $this->ticketCategoryValues[] = $ticketCategoryValue;
        }

        return $this;
    }

    /**
     * Remove ticketCategoryValue
     *
     * @param \CoreBundle\Entity\CategoryValues $ticketCategoryValue CategoryValues to remove
     */
    public function removeTicketCategoryValue(\CoreBundle\Entity\CategoryValues $ticketCategoryValue)
    {
        $this->ticketCategoryValues->removeElement($ticketCategoryValue);
    }

    /**
     * Get ticketCategoryValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketCategoryValues()
    {
        return $this->ticketCategoryValues;
    }
    /**
     * @var integer number of seconds this ticket will take
     * @ApiMeta(description="Estimate in seconds of how much work is required for this Ticket")
     */
    private $secondsEstimate = 0;

    /**
     * @var \DateTime creation date of this ticket
     * @ApiMeta(description="Creation timestamp of this Ticket")
     */
    private $creationDateTime;

    /**
     * @var \DateTime due date of this ticket
     * @ApiMeta(description="Due timestamp for this Ticket")
     */
    private $dueDateTime;


    /**
     * Set secondsEstimate
     *
     * @param integer $secondsEstimate set the seconds estimated for this ticket
     *
     * @return Tickets
     */
    public function setSecondsEstimate($secondsEstimate)
    {
        $this->secondsEstimate = $secondsEstimate;

        return $this;
    }

    /**
     * Get secondsEstimate
     *
     * @return integer
     */
    public function getSecondsEstimate()
    {
        return $this->secondsEstimate;
    }

    /**
     * Set creationDateTime
     *
     * @param \DateTime $creationDateTime creation date and time
     *
     * @return Tickets
     */
    public function setCreationDateTime($creationDateTime)
    {
        $this->creationDateTime = $creationDateTime;

        return $this;
    }

    /**
     * Get creationDateTime
     *
     * @return \DateTime
     */
    public function getCreationDateTime()
    {
        return $this->creationDateTime;
    }

    /**
     * Set dueDateTime
     *
     * @param \DateTime $dueDateTime date this ticket is due
     *
     * @return Tickets
     */
    public function setDueDateTime($dueDateTime)
    {
        $this->dueDateTime = $dueDateTime;

        return $this;
    }

    /**
     * Get dueDateTime
     *
     * @return \DateTime
     */
    public function getDueDateTime()
    {
        return $this->dueDateTime;
    }

    /**
     * Get ticketUsers as array
     *
     * @return array
     */
    public function getTicketUsersArray()
    {
        $ids = [];
        foreach($this->ticketUsers AS $user){
            $ids[] = $user->getId();
        }
        return $ids;
    }

    /**
     * Get ticketGroups as array
     *
     * @return array
     */
    public function getTicketGroupsArray()
    {
        $ids = [];
        foreach($this->ticketGroups AS $group){
            $ids[] = $group->getId();
        }
        return $ids;
    }
    /**
     * @var \CoreBundle\Entity\Users
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $users;


    /**
     * Set users
     *
     * @param \CoreBundle\Entity\Users $users
     *
     * @return Tickets
     */
    public function setUsers(\CoreBundle\Entity\Users $users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \CoreBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }
}
