<?php
/**
 * Class UserSessions
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */
namespace CoreBundle\Entity;

/**
 * UserSessions
 */
class UserSessions
{
    /**
     * @var string session data
     */
    private $sessData;

    /**
     * @var integer session time
     */
    private $sessTime;

    /**
     * @var integer length of session
     */
    private $sessLifetime;

    /**
     * @var binary id of session
     */
    private $sessId;


    /**
     * Set sessData
     *
     * @param string $sessData session data to set
     *
     * @return UserSessions
     */
    public function setSessData($sessData)
    {
        $this->sessData = $sessData;

        return $this;
    }

    /**
     * Get sessData
     *
     * @return string
     */
    public function getSessData()
    {
        return $this->sessData;
    }

    /**
     * Set sessTime
     *
     * @param integer $sessTime creation time for session
     *
     * @return UserSessions
     */
    public function setSessTime($sessTime)
    {
        $this->sessTime = $sessTime;

        return $this;
    }

    /**
     * Get sessTime
     *
     * @return integer
     */
    public function getSessTime()
    {
        return $this->sessTime;
    }

    /**
     * Set sessLifetime
     *
     * @param integer $sessLifetime length of time for session
     *
     * @return UserSessions
     */
    public function setSessLifetime($sessLifetime)
    {
        $this->sessLifetime = $sessLifetime;

        return $this;
    }

    /**
     * Get sessLifetime
     *
     * @return integer
     */
    public function getSessLifetime()
    {
        return $this->sessLifetime;
    }

    /**
     * Get sessId
     *
     * @return binary
     */
    public function getSessId()
    {
        return $this->sessId;
    }
}
