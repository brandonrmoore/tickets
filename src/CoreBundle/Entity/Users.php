<?php
/**
 * Class Users
 *
 * @package     CoreBundle
 * @subpackage  Entity
 */

namespace CoreBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use CoreBundle\Annotations\ApiMeta as ApiMeta;
use JMS\Serializer\Annotation AS JMSAnnotation;
/**
 * Users
 */
class Users implements UserInterface, \Serializable
{
    /**
     * @var string username of user
     * @ApiMeta(description="username for this User")
     */
    private $username;

    /**
     * @var string password of user
     * @JMSAnnotation\Exclude()
     * @ApiMeta(description="hashed password for this User")
     */
    private $password;

    /**
     * @var string email of user
     * @ApiMeta(description="email address for this User")
     */
    private $email;

    /**
     * @var boolean whether the user is active or not. True is active
     * @ApiMeta(description="active status for this User")
     */
    private $isActive;

    /**
     * @var string first name of user
     * @ApiMeta(description="first name of this User")
     */
    private $firstName;

    /**
     * @var string last name of user
     * @ApiMeta(description="last name for this User")
     */
    private $lastName;

    /**
     * @var \DateTime last date of any activity by this user
     * @ApiMeta(description="timestamp for last login by this User")
     */
    private $lastActivity;

    /**
     * @var boolean whether the user is an admin or not. True is admin
     * @ApiMeta(description="admin permissions granted for this User")
     */
    private $isAdmin;

    /**
     * @var integer auto incrementing id for this entity
     * @ApiMeta(description="Primary key")
     */
    private $id;


    /**
     * Set username
     *
     * @param string $username username for user
     *
     * @return Users
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password bcrypt hashed password
     *
     * @return Users
     */
    public function setPassword($password)
    {
        if($password){
            $this->password = $password;
        }


        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email email of user
     *
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive is this user enabled
     *
     * @return Users
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set firstName
     *
     * @param string $firstName first name of user
     *
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName last name of user
     *
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastActivity
     *
     * @param \DateTime $lastActivity date of last login for user
     *
     * @return Users
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin is this user an admin
     *
     * @return Users
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get User Roles
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = array('ROLE_USER');

        if($this->isAdmin) $roles[] = 'ROLE_ADMIN';
        return $roles;
    }

    /**
     * Serialize data
     *
     * @return string
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * Unserialize data
     *
     * @param string $serialized Serialized user data used for authentication
     * @return array
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized);
    }

    /**
     * Get Salt
     *
     * Dummy function expected to be their by UserInterface
     *
     * @return null
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * Erase Credentials
     *
     * Dummy function expected to be their by UserInterface
     *
     * @return null
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection ticket attributes
     * @ApiMeta(description="TicketAttributes associated with this User")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $ticketAttributes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketAttributes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute associated ticket attribute
     *
     * @return Users
     */
    public function addTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes[] = $ticketAttribute;

        return $this;
    }

    /**
     * Remove ticketAttribute
     *
     * @param \CoreBundle\Entity\TicketAttributes $ticketAttribute associated ticket attribute
     */
    public function removeTicketAttribute(\CoreBundle\Entity\TicketAttributes $ticketAttribute)
    {
        $this->ticketAttributes->removeElement($ticketAttribute);
    }

    /**
     * Get ticketAttributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicketAttributes()
    {
        return $this->ticketAttributes;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection UserScrums
     * @ApiMeta(description="Scrums associated with this User")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $userScrums;

    /**
     * Add userScrum
     *
     * @param \CoreBundle\Entity\Scrums $userScrum associated scrum
     *
     * @return Users
     */
    public function addUserScrum(\CoreBundle\Entity\Scrums $userScrum)
    {
        if (!$this->userScrums || !$this->userScrums->contains($userScrum)) {
            $this->userScrums[] = $userScrum;
            $userScrum->addScrumUser($this);
        }
        return $this;
    }

    /**
     * Remove userScrum
     *
     * @param \CoreBundle\Entity\Scrums $userScrum associated scrum
     */
    public function removeUserScrum(\CoreBundle\Entity\Scrums $userScrum)
    {
        $this->userScrums->removeElement($userScrum);
        $userScrum->removeScrumUser($this);
    }

    /**
     * Get userScrums
     *
     * @return \Doctrine\Common\Collections\Collection associated scrums for this user
     */
    public function getUserScrums()
    {
        return $this->userScrums = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection UserGroups
     * @ApiMeta(description="Groups associated with this User")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $userGroups;


    /**
     * Add userGroup
     *
     * @param \CoreBundle\Entity\Groups $userGroup associated group for this user
     *
     * @return Users
     */
    public function addUserGroup(\CoreBundle\Entity\Groups $userGroup)
    {
        if (!$this->userGroups || !$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->addGroupUser($this);
        }
        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \CoreBundle\Entity\Groups $userGroup associated group for this user
     */
    public function removeUserGroup(\CoreBundle\Entity\Groups $userGroup)
    {
        $this->userGroups->removeElement($userGroup);
        $userGroup->removeGroupUser($this);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection associated groups for this user
     */
    public function getUserGroups()
    {
        return $this->userGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection tickets assigned to this user
     * @ApiMeta(description="Tickets associated with this User")
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $userTickets;


    /**
     * Add userTicket
     *
     * @param \CoreBundle\Entity\Tickets $userTicket
     *
     * @return Users
     */
    public function addUserTicket(\CoreBundle\Entity\Tickets $userTicket)
    {
        $this->userTickets[] = $userTicket;

        return $this;
    }

    /**
     * Remove userTicket
     *
     * @param \CoreBundle\Entity\Tickets $userTicket
     */
    public function removeUserTicket(\CoreBundle\Entity\Tickets $userTicket)
    {
        $this->userTickets->removeElement($userTicket);
    }

    /**
     * Get userTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserTickets()
    {
        return $this->userTickets;
    }

    /**
     * Get userScrums as array
     *
     * @return array
     */
    public function getUserScrumsArray()
    {
        $ids = [];
        foreach($this->userScrums AS $user){
            $ids[] = $user->getId();
        }
        return $ids;
    }

    /**
     * Get userScrums as array
     *
     * @return array
     */
    public function getUserGroupsArray()
    {
        $ids = [];
        foreach($this->userGroups AS $user){
            $ids[] = $user->getId();
        }
        return $ids;
    }
    /**
     * @var string md5 hash of file
     * @ApiMeta(description="avatar image file hash for this User")
     */
    private $avatar;


    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return Users
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Get avatar url
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        return '/avatars/'.$this->avatar;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     * @JMSAnnotation\MaxDepth(2)
     * @JMSAnnotation\SkipWhenEmpty()
     */
    private $tickets;


    /**
     * Add ticket
     *
     * @param \CoreBundle\Entity\Tickets $ticket
     *
     * @return Users
     */
    public function addTicket(\CoreBundle\Entity\Tickets $ticket)
    {
        $this->tickets[] = $ticket;

        return $this;
    }

    /**
     * Remove ticket
     *
     * @param \CoreBundle\Entity\Tickets $ticket
     */
    public function removeTicket(\CoreBundle\Entity\Tickets $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }
}
