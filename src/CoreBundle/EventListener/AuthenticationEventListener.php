<?php
/**
* Class ExtendedManager
*
* @package     CoreBundle
* @subpackage  Model
*/
namespace CoreBundle\EventListener;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
/**
* AuthenticationEventListener
*
* This class is meant to perform actions on a successful login
*/
class AuthenticationEventListener implements AuthenticationSuccessHandlerInterface
{
    protected $router;
    protected $container;
    protected $em;

    public function __construct(Router $router, $container)
    {
        $this->router = $router;
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getEntityManager();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $user->setLastActivity(new \DateTime('now'));
        $this->em->persist($user);
        $this->em->flush();
        return new RedirectResponse('/');
    }

}