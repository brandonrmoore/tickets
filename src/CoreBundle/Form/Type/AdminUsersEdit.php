<?php
/**
 * Class AdminUsersEdit
 *
 * @package     CoreBundle
 * @subpackage  Form\Type
 */
namespace CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AdminUsersEdit
 *
 * Generates a form for the admin roles
 */
class AdminUsersEdit extends AbstractType
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Controller\Controller $container The symfony container object
     */
    private $container;
    /**
     * @var \Doctrine\ORM\EntityManager $em The Doctrine entity manager object
     */
    private $em;
    /**
     * @var \CoreBundle\Entity\Users $user The user entity for this user using the form
     */
    private $user;

    /**
     * Build the form
     *
     * @category Form Builders
     * @param \Symfony\Component\Form\FormBuilderInterface $builder Form Builder interface for SYmfony
     * @param array $options Options to be passed into the form builder
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $this->container = $options['container'];
        $this->em = $options['entity_manager'];
        $entity = $builder->getData();
        $builder
            ->add('username', TextType::class, array('label' => 'Username'))
            ->add('password', PasswordType::class, array(
                'required' => false,
                'label' => 'Password',
            ))
            ->add('email', TextType::class, array('label' => 'Email Address'))
            ->add('firstName', TextType::class, array('label' => 'First Name'))
            ->add('lastName', TextType::class, array('label' => 'Last Name'))
            ->add('isAdmin', CheckboxType::class, array('label' => 'Admin User'))
            ->add('isActive', CheckboxType::class, array('label' => 'Enabled'))
            ->add('userGroups', EntityType::class,
                array(
                    'class' => 'CoreBundle:Groups',
                    'choice_label' => 'name',
                    'choice_value' => 'id',
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false,
                    'label' => 'User Groups',
                    'data' => $entity ? $this->getCurrentUserGroups($entity->getId()) : null
                )
            )
            ->add('userScrums', EntityType::class,
                array(
                    'class' => 'CoreBundle:Scrums',
                    'choice_label' => 'name',
                    'choice_value' => 'id',
                    'multiple' => true,
                    'label' => 'User Scrums',
                    'expanded' => true,
                    'required' => false,
                    'data' => $entity ? $this->getCurrentUserScrums($entity->getId()) : null
                )
            )
            ->add('avatar', FileType::class, array(
                'mapped' => false,
                'required' => false
            ))
        ;
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            if(method_exists($this, 'preSubmit')){
                $this->preSubmit($event);
            }
        });
    }

    /**
     * Get user groups for supplied user entity
     *
     * @param \CoreBundle\Entity\Users $user The user entity to get groups for
     * @return array
     */
    private function getCurrentUserGroups($user)
    {
        return $this->em->getRepository('CoreBundle:Users')->findUserGroups($user);
    }

    /**
     * Get user scrums for supplied user entity
     *
     * @param \CoreBundle\Entity\Users $user The user entity to get groups for
     * @return array
     */
    private function getCurrentUserScrums($user)
    {
        return $this->em->getRepository('CoreBundle:Users')->findUserScrums($user);
    }

    /**
     * Perform presave form actions
     *
     * @param \Symfony\Component\Form\FormEvent $event Form event for this form
     * @return null
     */
    public function preSubmit($event)
    {
        $data = $event->getData();
        if($data->getPassword()){
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($data, $data->getPassword());
            $data->setPassword($password);
        }
        if(isset($_FILES['avatar'])){
            $uploaderService = $this->container->get('attachment.uploader');
            if($data->getAvatar()){
                $uploaderService->delete($data->getAvatar());
            }
            $fileName = $uploaderService->upload($_FILES['avatar']);
            $data->setAvatar($fileName);
        }
    }

    /**
     * Prepend form elements with this string
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver default options resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'buttons'        => array(),
            'options'        => array(),
            'mapped'         => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'form_actions';
    }

    /**
     * Handle options and set required ones
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     * @return null
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Users'
        ));
        $resolver->setRequired('container');
        $resolver->setRequired('entity_manager');
        $resolver->setRequired('user');
    }
}
