<?php
/**
 * Class ExtendedManager
 *
 * @package     CoreBundle
 * @subpackage  Model
 */
namespace CoreBundle\Model;

/**
 * ExtendedManager
 *
 * This class is meant to add in additional functionality to \Doctrine\ORM\EntityManager
 */
class ExtendedManager {

    /**
     * @var \Doctrine\ORM\EntityManager $entityManager Doctrine Entity Manager
     */
    protected $entityManager;

    /**
     * Constructor
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Set query where and order statements
     *
     * @param \Doctrine\ORM\QueryBuilder $query array of parameters to bind to the query
     * @param array $binds array of parameters to bind to the query
     * @param \CoreBundle\Entity $entity An entity under this namespace
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function doBinds($query, $binds, $entity)
    {
        foreach($binds AS $k=>$v){
            $k = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $k))));
            switch($k){
                case "page":
                case "limit":
                    continue;
                case "asc":
                    $v = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $v))));
                    $query->orderBy($entity.'.'.$v, 'ASC');
                    break;
                case "desc":
                    $v = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $v))));
                    $query->orderBy($entity.'.'.$v, 'DESC');
                    break;
                default:
                    $query->andWhere($entity.'.'.$k.' = :'.$k);
                    $query->setParameter($k, $v);
                    break;
            }
        }
        return $query;
    }
}