<?php
/**
 * Class ManagerDocumentation
 *
 * @package     CoreBundle
 * @subpackage  Model
 */
namespace CoreBundle\Model;

use Doctrine\Common\Annotations\AnnotationReader;
use CoreBundle\Entity\Categories;
use CoreBundle\Entity\CategoryValues;
use CoreBundle\Entity\Groups;
use CoreBundle\Entity\Scrums;
use CoreBundle\Entity\TicketAttributes;
use CoreBundle\Entity\TicketAttributeTypes;
use CoreBundle\Entity\Tickets;
use CoreBundle\Entity\TicketWorkflows;
use CoreBundle\Entity\Users;
/**
 * ManagerDocumentation
 *
 * This class is meant to add in additional functionality to \Doctrine\ORM\EntityManager
 */
class ManagerDocumentation {

    /**
     * @var \Doctrine\ORM\EntityManager $entityManager Doctrine Entity Manager
     */
    protected $entityManager;

    /**
     * Constructor
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    public function adminApiV1Documentation()
    {
        $doc = [];
        $doc['Introduction'] = $this->getIntroduction();
        $doc['Entities'] = $this->getEntityDocumentation();
        return $doc;
    }

    private function getIntroduction()
    {
        $intro = [];
        $intro[] = 'There are a few concepts to understand in order to use the API. First and foremost, the API is a direct representation of the stored data. In other words, there are no static hooks for the API. If an entity or form exists, such as Users or Tickets, then it can be queried directly.';
        $intro[] = 'Furthermore, some of the API calls are determine by whether the querying user has admin access or not. For example, if you try to query "/v1/admin/get/data/users" and your API token or current user does not have admin access, you will be redirected to a login page.';
        $intro[] = 'The API points can be broken down to grabbing data, grabbing a form, and posting a form.';
        return $intro;
    }

    private function getEntityDocumentation()
    {
        $docReader = new AnnotationReader();
        $entities = [];
        $metadata = $this->entityManager->getMetadataFactory()->getAllMetadata();
        foreach($metadata AS $m){
            //skip some entities
            if($m->name === 'CoreBundle\Entity\UserSessions'){
                continue;
            }
            $entities[$m->name] = [
                'name' => str_replace('CoreBundle\Entity\\', '',$m->name),
                'apiName' => str_replace('_','-',strtolower(
                    preg_replace(
                        ["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"],
                        ["_$1", "_$1_$2"],
                        lcfirst(str_replace('CoreBundle\Entity\\', '',$m->name))
                    )
                )),
                'fields' => [],
                'associationFields' => []
            ];
            $fullClassName = '\\'.$m->name;
            $entity = new $fullClassName();
            foreach($m->fieldMappings AS $field => $details){
                $entities[$m->name]['fields'][$field] = [
                    'type' => $details['type'],
                    'description' => ''
                ];
                $reflectionProperty = new \ReflectionProperty($entity, $field);
                $docInfos = $docReader->getPropertyAnnotations($reflectionProperty);
                if($docInfos){
                    $entities[$m->name]['fields'][$field]['description'] = $docInfos[0]->description;
                }
            }
            foreach($m->associationMappings AS $field => $details){
                if(!$details['isOwningSide']) {
                    continue;
                }
                $entities[$m->name]['associationFields'][$field] = [
                    'targetEntity' => str_replace('CoreBundle\Entity\\', '',$details['targetEntity'])
                ];
                if(isset($details['joinTable'])){
                    $entities[$m->name]['associationFields'][$field]['type'] = 'many-to-many';
                }
                if(isset($details['joinColumns'])){
                    $entities[$m->name]['associationFields'][$field]['type'] = 'many-to-one';
                }
                $reflectionProperty = new \ReflectionProperty($entity, $field);
                $docInfos = $docReader->getPropertyAnnotations($reflectionProperty);
                if($docInfos){
                    $entities[$m->name]['associationFields'][$field]['description'] = $docInfos[0]->description;
                }
            }
        }
        ksort($entities);
        return array_values($entities);
    }
}