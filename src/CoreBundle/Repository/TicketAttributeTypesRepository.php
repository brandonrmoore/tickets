<?php
/**
 * Class TicketAttributeTypesRepository
 *
 * @package     CoreBundle
 * @subpackage  Repository
 */
namespace CoreBundle\Repository;

/**
 * TicketAttributeTypesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TicketAttributeTypesRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get data for user roles
     *
     * @param array $binds array of parameters to bind to the query
     * @param \CoreBundle\Model\ExtendedManager $extendedManager extension of the entity manager
     *
     * @return array
     */
    public function getUserData($binds, $extendedManager)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->from('CoreBundle\Entity\TicketAttributeTypes', 'ticketAttributeTypes')
            ->select('ticketAttributeTypes.name')
            ->addSelect('ticketAttributeTypes.id')
            ->setMaxResults($binds['limit'])
            ->setFirstResult( $binds['page'] * $binds['limit'] );
        $query = $extendedManager->doBinds($query, $binds, 'ticketAttributeTypes');
        $qb = $query->getQuery();
        try {
            return $qb->getScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
