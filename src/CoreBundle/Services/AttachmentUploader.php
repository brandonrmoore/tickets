<?php
/**
 * Class AttachmentUploader
 *
 * @package     CoreBundle
 * @subpackage  Services
 */
namespace CoreBundle\Services;

use ElasticSearch\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AttachmentUploader
 *
 * Handle all file uploads
 *
 */
class AttachmentUploader
{

    /**
     * Handle uploading of files
     *
     * To ensure all files end up in the same format, use this function to handle file uploads
     *
     * @category Services
     * @param array $file This should be what is contained in $_FILES['attachment']
     *
     * @return string The filename used
     */
    public function upload($file)
    {
        try {
            $fileName = md5(uniqid().$file['name']);
            $parentDir = 'uploads';
            $firstChildDir = substr($fileName,0,1);
            $secondChildDir = substr($fileName,0,2);
            $doc_root = str_replace('/web', '', $_SERVER['DOCUMENT_ROOT']);
            $new_file_path = $doc_root.'/'.$parentDir.'/'.$firstChildDir.'/'.$secondChildDir;
            mkdir($new_file_path, 0777, true);
            move_uploaded_file($file['tmp_name'], $new_file_path.'/'.$fileName);
            return $fileName;
        }catch(Exception $e){
            throw new NotFoundHttpException("Error in uploading file. Be sure to check permissions");
        }
    }

    /**
     * Handle downloading of files
     *
     * Ensures the correct route will always be derived so long as any controller refers to this function
     *
     * @category Services
     * @param string $fileName This should be the md5 hash of the file
     *
     * @return string The full path for the file
     */
    public function getPath($fileName)
    {
        $parentDir = 'uploads';
        $firstChildDir = substr($fileName,0,1);
        $secondChildDir = substr($fileName,0,2);
        $doc_root = str_replace('/web', '', $_SERVER['DOCUMENT_ROOT']);
        $new_file_path = $doc_root.'/'.$parentDir.'/'.$firstChildDir.'/'.$secondChildDir;
        $fullPath = $new_file_path.'/'.$fileName;
        if(file_exists($fullPath)){
            return $fullPath;
        }else{
            throw new NotFoundHttpException("File not found");
        }
    }

    /**
     * Handle deleting of files
     *
     * Finds the full path and unlinks the file
     *
     * @category Services
     * @param string $fileName This should be the md5 hash of the file
     *
     * @return bool true if successfully unlinked
     */
    public function delete($fileName)
    {
        $fullPath = $this->getPath($fileName);
        if($fullPath && file_exists($fullPath)){
            try {
                unlink($fullPath);
            }catch(Exception $e){
                die();
            }
        }
    }
}