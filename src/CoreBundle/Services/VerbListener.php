<?php
/**
 * Class VerbListener
 *
 * @package     CoreBundle
 * @subpackage  Services
 */
namespace CoreBundle\Services;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Class AttachmentUploader
 *
 * Handle all file uploads
 *
 */
class VerbListener
{
    public function onKernelRequest ( GetResponseEvent $getEvent )
    {
        $request = $getEvent->getRequest();
        if ( $request->query->get('_method') ) {
            $request->setMethod( $request->query->get('_method') );
        }
        if ( $request->get('_method') ) {
            $request->setMethod( $request->get('_method') );
        }
    }
}