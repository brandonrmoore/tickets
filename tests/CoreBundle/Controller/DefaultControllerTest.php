<?php
namespace Tests\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\CoreBundle\DatabasePrimer;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Finder\Finder;
use Tests\CoreBundle\Utils\LoginUtils;

class DefaultControllerTest extends WebTestCase
{
    private $client = null;
    private $entityPaths = [];

    public function setUp()
    {
        self::bootKernel();

        DatabasePrimer::prime(self::$kernel);
        $this->client = static::createClient();
    }

    public function testIndex()
    {
        $this->client = static::createClient();
        $crawler = $this->client->request(
            'GET',
            '/'
        );

        $this->assertContains(
            '<app-root>',
            $this->client->getResponse()->getContent(),
            $this->failureMessage(__FUNCTION__)
        );
    }

//    public function testAdminData()
//    {
//        $em = $this->client->getContainer()->get('doctrine')->getManager();
//        $tables = $em->getConnection()->getSchemaManager()->listTables();
//        $namespaces = $em->getConfiguration()->getEntityNamespaces();
//        foreach ($tables as $table) {
//            foreach ($namespaces as $namespace) {
//                if (class_exists($namespace.'\\'.$this->entityMethod($table->getName())) && $table->getName() !== 'user_sessions') {
//                    $this->entityPaths[] = str_replace('_','-',$table->getName());
//                    break;
//                }
//            }
//
//        }
//        $this->client = static::createClient();
//        $this->client->enableProfiler();
//        $loginUtils = new LoginUtils($this->client);
//        $loginUtils->adminLogIn();
//        //test each entity paths for order, page, limit, and id key values
//        foreach($this->entityPaths AS $entityPath){
//            $entity = $this->client->getContainer()
//                ->get('doctrine')
//                ->getManager()
//                ->getRepository('CoreBundle:'.$this->entityMethod($entityPath))
//                ->findOneBy([]);
//            $url = '/v1/admin/get/data/'.$entityPath.'/page/0/limit/1/asc/id';
//            if($entity){
//                $url .= '/id/'.$entity->getId();
//            }
//            $crawler = $this->client->request(
//                'GET',
//                $url
//            );
//            if($profile = $this->client->getProfile()){
//                // check the number of requests
//                $this->assertLessThan(
//                    5,
//                    $profile->getCollector('db')->getQueryCount()
//                );
//
//                // check the time spent in the framework
//                $this->assertLessThan(
//                    500,
//                    $profile->getCollector('time')->getDuration()
//                );
//            }
//            $this->assertTrue(
//                $this->client->getResponse()->headers->contains(
//                    'Content-Type',
//                    'application/json'
//                ),
//                $this->failureMessage(__FUNCTION__, "Failed run GET on $url")
//            );
//        }
//    }

//    public function testForm()
//    {
//        $this->client = static::createClient();
//        $this->client->enableProfiler();
//        $loginUtils = new LoginUtils($this->client);
//        $loginUtils->adminLogIn();
//        $finder = new Finder();
//        $finder->files()->in($_SERVER['PWD'].'/src/CoreBundle/Form/Type');
//
//        foreach ($finder as $file) {
//
//            $string = str_replace('.php','',$file->getRelativePathname());
//            $clean = '';
//            $len = strlen($string); //Need to get it again if shorter
//            for ($counter = 0; $counter < $len; $counter++) {
//                if (strtoupper($string[$counter]) == $string[$counter])
//                    $clean .= ' ';
//                $clean .= $string[$counter];
//            }
//            $clean = strtolower($clean);
//            $parts = explode(' ',trim($clean));
//            if(count($parts) > 3){
//                $tmpPart = '';
//                for($i = 1; $i < count($parts) - 1; $i++){
//                    if($i !== 1){
//                        $tmpPart .= '-';
//                    }
//                    $tmpPart .= $parts[$i];
//                }
//                $parts[1] = $tmpPart;
//            }
//            $url = '/v1/'.$parts[0].'/get/form/'.$parts[1].'/'.$parts[(count($parts) - 1)];
//            $crawler = $this->client->request(
//                'GET',
//                $url
//            );
//            if($profile = $this->client->getProfile()){
//                // check the number of requests
//                $this->assertLessThan(
//                    5,
//                    $profile->getCollector('db')->getQueryCount()
//                );
//
//                // check the time spent in the framework
//                $this->assertLessThan(
//                    500,
//                    $profile->getCollector('time')->getDuration()
//                );
//            }
//            $this->assertTrue(
//                $this->client->getResponse()->headers->contains(
//                    'Content-Type',
//                    'application/json'
//                ),
//                $this->failureMessage(__FUNCTION__, "Failed run GET on $url")
//            );
//        }
//    }
//
//    public function testFormSubmit()
//    {
//        $this->client = static::createClient();
//        $this->client->enableProfiler();
//        $loginUtils = new LoginUtils($this->client);
//        $loginUtils->adminLogIn();
//        $finder = new Finder();
//        $finder->files()->in($_SERVER['PWD'].'/src/CoreBundle/Form/Type');
//
//        foreach ($finder as $file) {
//            $formData = [
//                'unitTest' => 1
//            ];
//            $string = str_replace('.php', '', $file->getRelativePathname());
//            $clean = '';
//            $len = strlen($string); //Need to get it again if shorter
//            for ($counter = 0; $counter < $len; $counter++) {
//                if (strtoupper($string[$counter]) == $string[$counter])
//                    $clean .= ' ';
//                $clean .= $string[$counter];
//            }
//            $clean = strtolower($clean);
//            $parts = explode(' ', trim($clean));
//            if (count($parts) > 3) {
//                $tmpPart = '';
//                for ($i = 1; $i < count($parts) - 1; $i++) {
//                    if ($i !== 1) {
//                        $tmpPart .= '-';
//                    }
//                    $tmpPart .= $parts[$i];
//                }
//                $parts[1] = $tmpPart;
//            }
//            $url = '/v1/' . $parts[0] . '/get/form/' . $parts[1] . '/' . $parts[(count($parts) - 1)];
//            $crawler = $this->client->request(
//                'GET',
//                $url
//            );
//            $content = json_decode($this->client->getResponse()->getContent());
//            foreach($content AS $c) {
//                if($c->name == 'action'){
//                    $submitUrl = $c->value;
//                    continue;
//                }
//                $formData[$c->name] = $this->getRandomValue($c->type);
//            }
//            $_REQUEST = $formData;
//            $crawler = $this->client->request(
//                'POST',
//                $submitUrl,
//                $formData
//            );
//            $this->assertTrue(
//                $this->client->getResponse()->headers->contains(
//                    'Content-Type',
//                    'application/json'
//                ),
//                $this->failureMessage(__FUNCTION__, "Failed run POST on $submitUrl")
//            );
//        }
//    }

//    public function testUserData()
//    {
//        $em = $this->client->getContainer()->get('doctrine')->getManager();
//        $tables = $em->getConnection()->getSchemaManager()->listTables();
//        $namespaces = $em->getConfiguration()->getEntityNamespaces();
//        foreach ($tables as $table) {
//            foreach ($namespaces as $namespace) {
//                if (class_exists($namespace.'\\'.$this->entityMethod($table->getName())) && $table->getName() !== 'user_sessions') {
//                    $this->entityPaths[] = str_replace('_','-',$table->getName());
//                    break;
//                }
//            }
//
//        }
//        $this->client = static::createClient();
//        $this->client->enableProfiler();
//        $loginUtils = new LoginUtils($this->client);
//        $loginUtils->adminLogIn();
//        //test each entity paths for order, page, limit, and id key values
//        foreach($this->entityPaths AS $entityPath){
//            $entity = $this->client->getContainer()
//                ->get('doctrine')
//                ->getManager()
//                ->getRepository('CoreBundle:'.$this->entityMethod($entityPath))
//                ->findOneBy([]);
//            $url = '/v1/user/get/data/'.$entityPath.'/page/0/limit/1/asc/id';
//            if($entity){
//                $url .= '/id/'.$entity->getId();
//            }
//            $crawler = $this->client->request(
//                'GET',
//                $url
//            );
//            if($profile = $this->client->getProfile()){
//                // check the number of requests
//                $this->assertLessThan(
//                    5,
//                    $profile->getCollector('db')->getQueryCount()
//                );
//
//                // check the time spent in the framework
//                $this->assertLessThan(
//                    500,
//                    $profile->getCollector('time')->getDuration()
//                );
//            }
//            switch($entityPath){
//                default:
//                    $this->assertTrue(
//                        $this->client->getResponse()->headers->contains(
//                            'Content-Type',
//                            'application/json'
//                        ),
//                        $this->failureMessage(__FUNCTION__, "Failed run GET on $url")
//                    );
//                    break;
//
//            }
//        }
//    }

    private function failureMessage($function, $moreText = false)
    {
        return "\n\n=============================================\n\n".
            "TEST FAILED IN ".__FILE__." at function ".$function. ($moreText ? "\n$moreText" : '').
            "\n\n=============================================\n\n";
    }

    private function getRandomValue($type)
    {
        switch($type){
            case "entity":
            case "number":
                return rand(1,100);
            case "checkbox":
                return rand(0,1);
            case "text":
                return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
            case "password":
                return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(26/strlen($x)) )),1,26);
            case "date":
                return date('Y-m-d H:i:s', strtotime( '+'.mt_rand(0,30).' days'));
        }
    }

    private function entityMethod($entityPath)
    {
        return str_replace(' ', '', ucwords(str_replace('_',' ',str_replace('-', ' ', $entityPath))));
    }
}
